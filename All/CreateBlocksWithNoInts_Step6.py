#-----------------------------------------------------------------------
#This script dynamically creates a Blocks that do not intersect with 
#other segments, and adds them to the Block Table
#-----------------------------------------------------------------------

import arcpy, os, sys
import requests
import numpy
import operator
import math


scriptloc = sys.path[0]
os.chdir(scriptloc)
gdb = "scratch.gdb"
scratchgdb = os.path.join(scriptloc, gdb)

connection = r"Database Connections\DDOTLRS as LRSVIEWER.sde"
connectiondev = r"Database Connections\DDOTGISDEV as RH user.sde"

roadgap = os.path.join(scratchgdb, 'RH_RoadwayGaps')
routeintfc = os.path.join(connection, 'RH.LRSI_RouteIntersection')
rhrdwyint = os.path.join(scratchgdb, 'RH_BlockIntersections')
rdwy = os.path.join(connection, 'Ddotlrs.RH.LRSN_Roadway')
enhancedsegtbl = os.path.join(scratchgdb, 'RH_BlockTable')
sbblkerror = os.path.join(scratchgdb, 'SubBlockErrorTable')
stsegments = os.path.join(connection, 'Ddotlrs.RH.LRSE_StreetSegment')


with arcpy.da.SearchCursor(rdwy, ['ROUTEID','SHAPE.STLength()','ROUTENAME'], where_clause="(FROMDATE is null or FROMDATE<=CURRENT_TIMESTAMP) and (TODATE is null or TODATE>CURRENT_TIMESTAMP)") as scursor2:
    rdwydic = {}
    for row in scursor2:
        rdwydic[row[0]] = {}
        rdwydic[row[0]]['Length'] = row[1]
        rdwydic[row[0]]['RouteName'] = row[2]

del scursor2

blocklist = []
with arcpy.da.SearchCursor(enhancedsegtbl, ['RouteID']) as scursor:
    for row in scursor:
        print row
        blocklist.append(row[0])

intlist = []
with arcpy.da.SearchCursor(routeintfc, ['RouteID'],where_clause="(FROMDATE is null or FROMDATE<=CURRENT_TIMESTAMP) and (TODATE is null or TODATE>CURRENT_TIMESTAMP)") as scursor: 
    for row in scursor:
        if row[0][0] == '1' and row[0] not in blocklist:
            intlist.append(row[0])
            
#UPDATING TABLE
with arcpy.da.InsertCursor(enhancedsegtbl, ['RouteID','FromMeasure','ToMeasure','FromIntersectionID', 'ToIntersectionID','RouteName']) as icursor:    
    for intrtid in intlist:
        rtlngth = rdwydic[intrtid]['Length']
        rtname = rdwydic[intrtid]['RouteName']
        icursor.insertRow((intrtid, 0, rtlngth, 'Dead End', 'Dead End', rtname))
                







