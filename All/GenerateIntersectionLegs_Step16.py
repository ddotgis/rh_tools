#------------------------------------------------
#Generates intersection legs at each intersection.
#------------------------------------------------
import arcpy, os, sys
from pprint import pprint
import requests
import numpy
import operator
import math
from math import *

scriptloc = sys.path[0]
os.chdir(scriptloc)
gdb = "scratch.gdb"
scratchgdb = os.path.join(scriptloc, gdb)

connection = r"Database Connections\DDOTLRS as LRSVIEWER.sde"
connectiondev = r"Database Connections\DDOTGISDEV as RH user.sde"

rdwy = os.path.join(connection, 'Ddotlrs.RH.LRSN_Roadway')
routeintfc = os.path.join(connection, 'RH.LRSI_RouteIntersection')
intapproaches = os.path.join(scratchgdb, 'RH_Approaches')
rhrdwyseg = os.path.join(scratchgdb, 'RH_SubBlock')

if not arcpy.Exists(scratchgdb):
    arcpy.CreateFileGDB_management(scriptloc, os.path.basename(scratchgdb))

tablename = 'RH_Approaches'
if not arcpy.Exists(intapproaches):
    arcpy.CreateTable_management(scratchgdb, tablename)
    arcpy.AddField_management(intapproaches, "RouteID", "TEXT", 50)
    arcpy.AddField_management(intapproaches, "ApproachID", "SHORT", 50)   
    arcpy.AddField_management(intapproaches, "SubBlockID", "TEXT", 50)
    arcpy.AddField_management(intapproaches, "IntersectionID", "TEXT", 80)
    arcpy.AddField_management(intapproaches, "FromMeasure", "FLOAT", 50)
    arcpy.AddField_management(intapproaches, "ToMeasure", "FLOAT", 50)
    arcpy.AddField_management(intapproaches, "Angle", "TEXT", 50)
    arcpy.AddField_management(intapproaches, "Directionality", "TEXT", 50)
    arcpy.AddField_management(intapproaches, "RoadType", "TEXT", 50)
    arcpy.AddField_management(intapproaches, "IntersectionDirection", "SHORT", 80)
else:
    arcpy.TruncateTable_management(intapproaches)

with arcpy.da.SearchCursor(routeintfc, ['ROUTEID','Measure'],where_clause="(FromDate is null or FromDate<=CURRENT_TIMESTAMP) and (ToDate is null or ToDate>CURRENT_TIMESTAMP)") as scursor:
    intdic = {}
    for row in scursor:
        if row[0] in intdic:
            intdic[row[0]].append(row[1])
        else:
            intdic[row[0]] = [row[1]]

with arcpy.da.SearchCursor(rdwy, ['ROUTEID','ROADTYPE'],where_clause="(FromDate is null or FromDate<=CURRENT_TIMESTAMP) and (ToDate is null or ToDate>CURRENT_TIMESTAMP)") as scursor:
    rdtypedic = {}
    for row in scursor:
        rdtypedic[row[0]] = row[1]

#APPROACH LENGTH (CAN BE CHANGED)
applen = 10

#UPDATING TABLE               
with arcpy.da.InsertCursor(intapproaches, ['RouteID','SubBlockID','IntersectionID','FromMeasure','ToMeasure','Angle','Directionality','RoadType']) as icursor:
    with arcpy.da.SearchCursor(rhrdwyseg, ['RouteID','SubBlockID','FromMeasure','ToMeasure','FromIntersectionID','ToIntersectionID']) as scursor:
        
        for row in scursor:
            print row[0], row[1], row[2]
            rtid = row[0]
            sblockid = row[1]
            fmeas = float(row[2])
            tmeas = float(row[3])
            fintid = row[4]
            tintid = row[5]
            rdtype = rdtypedic[rtid]

            try:
                intmeasurelist = intdic[rtid]
            except KeyError:
                print "Route does not contain any intersections"

            #If one of the ends is a 'Dead End'
            if fintid == 'Dead End' or tintid == 'Dead End':
                
                #If the fintid is 'Dead End'. This indicates that the intersection is at the end of the segment.
                if fintid == 'Dead End':
                    
                    #If the tmeas is enough distance away from the fmeas to create legs that are applen long.
                    if tmeas >= (fmeas + applen):
                        
                        #APPROACH
                        setvar = 0
                        for i in intmeasurelist:
                            if i <= (tmeas + .002) and i >= (tmeas - .002):
                                setvar = 1
                                break
                            else:
                                continue
                        if setvar == 1:
                            startmeas1 = (tmeas - applen)
                            endmeas1 = tmeas
                            whichint1 = tintid
                            rdseg1 = sblockid
                            icursor.insertRow((rtid, rdseg1, whichint1, startmeas1, endmeas1, None, None, rdtype))
                            
                    #If tmeas isn't enough distance away from fmeas to create legs that are applen long. In this case, we create shorter legs to compromise.
                    elif tmeas < (fmeas + applen):
                                        
                        newapplen = tmeas - fmeas
                        
                        #APPROACH
                        setvar = 0
                        for i in intmeasurelist:
                            if i <= (tmeas + .002) and i >= (tmeas - .002):
                                setvar = 1
                                break
                            else:
                                continue
                        if setvar == 1:
                            startmeas1 = (tmeas - newapplen)
                            endmeas1 = tmeas
                            whichint1 = tintid
                            rdseg1 = sblockid
                            icursor.insertRow((rtid, rdseg1, whichint1, startmeas1, endmeas1, None, None, rdtype))

                #If the tintid is 'Dead End'. This indicates that the intersection is at the beginning of the segment.
                elif tintid == 'Dead End':

                    #If the tmeas is enough distance away from the fmeas to create legs that are applen long.
                    if tmeas >= (fmeas + applen):

                        #APPROACH
                        setvar = 0
                        for i in intmeasurelist:
                            if i <= (fmeas + .002) and i >= (fmeas - .002):
                                setvar = 1
                                break
                            else:
                                continue
                        if setvar == 1:
                            startmeas1 = fmeas
                            endmeas1 = (fmeas + applen)
                            whichint1 = fintid
                            rdseg1 = sblockid
                            icursor.insertRow((rtid, rdseg1, whichint1, startmeas1, endmeas1, None, None, rdtype))

                    #If tmeas isn't enough distance away from fmeas to create legs that are applen long. In this case, we create shorter legs to compromise.
                    elif tmeas < (fmeas + applen):
                                        
                        newapplen = tmeas - fmeas
                        
                        #APPROACH
                        setvar = 0
                        for i in intmeasurelist:
                            if i <= (fmeas + .002) and i >= (fmeas - .002):
                                setvar = 1
                                break
                            else:
                                continue
                        if setvar == 1:
                            startmeas1 = fmeas
                            endmeas1 = (fmeas + newapplen)
                            whichint1 = fintid
                            rdseg1 = sblockid
                            icursor.insertRow((rtid, rdseg1, whichint1, startmeas1, endmeas1, None, None, rdtype))

            #If neither end is a 'Dead End'
            else:
                
                #If the tmeas is enough distance away from the fmeas to create legs that are applen long.
                if tmeas >= (fmeas + 2*(applen)):

                    #APPROACH 1
                    setvar = 0
                    for i in intmeasurelist:
                        if i <= (fmeas + .002) and i >= (fmeas - .002):
                            setvar = 1
                            break
                        else:
                            continue
                    if setvar == 1:
                        startmeas1 = fmeas
                        endmeas1 = (fmeas + applen)
                        whichint1 = fintid
                        rdseg1 = sblockid
                        icursor.insertRow((rtid, rdseg1, whichint1, startmeas1, endmeas1, None, None, rdtype))
                    
                    #APPROACH 2
                    setvar = 0
                    for i in intmeasurelist:
                        if i <= (tmeas + .002) and i >= (tmeas - .002):
                            setvar = 1
                            break
                        else:
                            continue
                    if setvar == 1:
                        startmeas2 = (tmeas - applen)
                        endmeas2 = tmeas
                        whichint2 = tintid
                        rdseg2 = sblockid
                        icursor.insertRow((rtid, rdseg2, whichint2, startmeas2, endmeas2, None, None, rdtype))
                    else:
                        continue

                #If tmeas isn't enough distance away from fmeas to create legs that are applen long. In this case, we create shorter legs to compromise.
                elif tmeas < (fmeas + (2 * applen)):

                    intdist = tmeas - fmeas
                    newapplen = (intdist/2)
                    
                    #APPROACH 1
                    setvar = 0
                    for i in intmeasurelist:
                        if i <= (fmeas + .002) and i >= (fmeas - .002):
                            setvar = 1
                            break
                        else:
                            continue
                    if setvar == 1:
                        startmeas1 = fmeas
                        endmeas1 = (fmeas + newapplen)
                        whichint1 = fintid
                        rdseg1 = sblockid
                        icursor.insertRow((rtid, rdseg1, whichint1, startmeas1, endmeas1, None, None, rdtype))
                   
                    #APPROACH 2
                    setvar = 0
                    for i in intmeasurelist:
                        if i <= (tmeas + .002) and i >= (tmeas - .002):
                            setvar = 1
                            break
                        else:
                            continue
                    if setvar == 1:
                        startmeas2 = (tmeas - newapplen)
                        endmeas2 = tmeas
                        whichint2 = tintid
                        rdseg2 = sblockid
                        icursor.insertRow((rtid, rdseg2, whichint2, startmeas2, endmeas2, None, None,rdtype))
                
print "FINISHED"











