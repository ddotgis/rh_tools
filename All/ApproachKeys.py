#_________________________________________________________________________________
#This script updates the table created in Step 1, filling in the null values with
#actual intersection IDs.
#
#This script is dependent on GenerateSegmentsToTable_Step1.py
#_________________________________________________________________________________
import arcpy, os
import os, sys
from pprint import pprint
import requests
import numpy
import operator
import math
import hashlib

scriptloc = sys.path[0]
os.chdir(scriptloc)
gdb = "scratch.gdb"
scratchgdb = os.path.join(scriptloc, gdb)
connection = r"Database Connections\DDOTLRS as LRSVIEWER.sde" 

rdwyseg = os.path.join(connection, 'Ddotlrs.RH.LRSE_RoadwaySegment')
approachFC = os.path.join(scratchgdb, 'RH_Approach')


print "Updating Approaches..."

with arcpy.da.UpdateCursor(approachFC, ['SubBlockID','IntersectionID','SubBlockKey','IntersectionKey']) as ucursor:
    for row in ucursor:
        if row[0] == None or row[0] == '':
            continue
        if row[1] == None or row[1] == '':
            continue
    #SubBlockID to SubBlockKey
        subblockHas = hashlib.md5(row[0])
        subblockKey = str(subblockHas.hexdigest())
        row[2] = subblockKey

    #IntersectionID to FromIntersectionKey
        intHas = hashlib.md5(row[1])
        intKey = str(intHas.hexdigest())
        row[3] = intKey

        ucursor.updateRow((row))
del ucursor











