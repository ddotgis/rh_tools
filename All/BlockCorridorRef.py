import arcpy, os, sys
import requests
import numpy
import operator
import math


scriptloc = sys.path[0]
os.chdir(scriptloc)
gdb = "scratch.gdb"
scratchgdb = os.path.join(scriptloc, gdb)

lrs_connection = r"Database Connections\DDOTLRS as LRSVIEWER.sde\Ddotlrs.RH.ReferenceFeatures"

blcks = os.path.join(lrs_connection, 'Ddotlrs.RH.Block')
corridors = os.path.join(scratchgdb, 'RH_CorridorTable')

#Create table and schema
tablename = 'BlockCorridorReference'
tableloc = os.path.join(scratchgdb, tablename)
if not arcpy.Exists(tableloc):
    arcpy.CreateTable_management(scratchgdb, tablename)
    arcpy.AddField_management(tableloc, "BlockID", "TEXT", 50)
    arcpy.AddField_management(tableloc, "BlockKey", "TEXT", 50)
    arcpy.AddField_management(tableloc, "CorridorID", "TEXT")
else:
    arcpy.TruncateTable_management(tableloc)

#Create corridor dictionary
corrDict = {}
with arcpy.da.SearchCursor(corridors, ['RouteID','CorridorID','FromMeasure','ToMeasure']) as scursor:
    for row in scursor:
        rtID = row[0]
        corrID = row[1]
        fmeas = row[2]
        tmeas = row[3]

        if rtID not in corrDict:
            corrDict[rtID] = {}
            corrDict[rtID][corrID] = {}
            corrDict[rtID][corrID]['FromMeasure'] = fmeas
            corrDict[rtID][corrID]['ToMeasure'] = tmeas
        else:
            corrDict[rtID][corrID] = {}
            corrDict[rtID][corrID]['FromMeasure'] = fmeas
            corrDict[rtID][corrID]['ToMeasure'] = tmeas          
del scursor

#Loop through every block and find matching Corridor
icursor = arcpy.da.InsertCursor(tableloc,['BlockID','BlockKey','CorridorID'])
with arcpy.da.SearchCursor(blcks, ['RouteID','FromMeasure','ToMeasure','BlockID','BlockKey']) as scursor:
    fitslist = []
    for row in scursor:
        rtID = row[0]
        fmeas = row[1]
        tmeas = row[2]
        blockID = row[3]
        blockKey = row[4]

        if blockKey == '8a22b07e11bb91c73a364cda903a1166':
            print "OK"
        fits = 0
        for corr in corrDict[rtID]:
            if fmeas >= (corrDict[rtID][corr]['FromMeasure'] - .1) and tmeas <= (corrDict[rtID][corr]['ToMeasure'] + .1):
                fits = 1
                icursor.insertRow((blockID, blockKey, corr))
                break
            else:
                continue
        if fits == 0:
            fitslist.append(blockKey)

del scursor
print str(len(fitslist))
print "DONE"














