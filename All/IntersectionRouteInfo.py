#-------------------------------------------------------------------------------------------------
#
#
#-------------------------------------------------------------------------------------------------
import arcpy, os, sys
import requests
import numpy
import operator
import math
import hashlib


scriptloc = sys.path[0]
gdb = "scratch.gdb"
scratchgdb = os.path.join(scriptloc, gdb)

connection = r"Database Connections\DDOTLRS as LRSVIEWER.sde"
connection2 = r"Database Connections\DDOTLRS as RH user.sde"

RH_roadway = os.path.join(connection, "Ddotlrs.RH.LRSN_Roadway")
RH_intersections = os.path.join(connection, "Ddotlrs.RH.LRSI_RouteIntersection")
RH_intersections_local = os.path.join(scratchgdb, 'RouteIntersections_local')

newfc = os.path.join(scratchgdb, 'RH_SubBlockIntersections')
newref = os.path.join(scratchgdb, 'RH_IntReference')

if not arcpy.Exists(newref):
    arcpy.CreateTable_management(scratchgdb, os.path.basename(newref))
    arcpy.AddField_management(newref, "INTERSECTIONID", "TEXT", 500, "", "", "", "NULLABLE", "REQUIRED")
    arcpy.AddField_management(newref, "ROUTEID", "TEXT", 500, "", "", "", "NULLABLE", "REQUIRED")
    arcpy.AddField_management(newref, "MEASURE", "FLOAT", '12', "3", "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(newref, "INTERSECTIONNAME", "TEXT", 500, "", "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(newref, "GRADE", "SHORT", '', "", "", "", "NULLABLE", "NON_REQUIRED",field_domain='AtGradeInt')
    arcpy.AddField_management(newref, "INTERSECTIONKEY", "TEXT", 100, "", "", "", "NULLABLE", "REQUIRED")
    arcpy.AddField_management(newref, "X_Coord", "DOUBLE", "", "", "", "", "NULLABLE", "REQUIRED")
    arcpy.AddField_management(newref, "Y_Coord", "DOUBLE", "", "", "", "", "NULLABLE", "REQUIRED")
    
else:
    arcpy.TruncateTable_management(newref)

icursor = arcpy.da.InsertCursor(newref, ['INTERSECTIONID','ROUTEID','MEASURE','INTERSECTIONNAME','GRADE','INTERSECTIONKEY','X_Coord','Y_Coord'])
with arcpy.da.SearchCursor(newfc, ['INTERSECTIONID','ROUTEID','MEASURE','INTERSECTIONNAME','GRADE','INTERSECTIONKEY','SHAPE@X','SHAPE@Y']) as scursor:
    for row in scursor:
        intid = row[0]
        rtid = row[1]
        intname = row[3]
        grade = row[4]
        intkey = row[5]
        xcoor = row[6]
        ycoor = row[7]

        spIntId = intid.split('_')
        for rte in spIntId:
            icursor.insertRow((intid,rte, None, intname, grade, intkey, xcoor, ycoor))
del icursor

spRef = r"Coordinate Systems\Projected Coordinate Systems\State Plane\NAD 1983 (Meters)\NAD 1983 StatePlane Maryland FIPS 1900 (Meters).prj"
if arcpy.Exists('TEMPINT'):
    arcpy.Delete_management('TEMPINT')
    arcpy.MakeXYEventLayer_management(newref, 'X_Coord', 'Y_Coord', 'TEMPINT', spRef)
else:
    arcpy.MakeXYEventLayer_management(newref, 'X_Coord', 'Y_Coord', 'TEMPINT', spRef)
finalref = os.path.join(scratchgdb, 'RH_IntReferenceFinal')

arcpy.MakeFeatureLayer_management(RH_intersections, "FilteredInts", where_clause="(FROMDATE is null or FROMDATE<=CURRENT_TIMESTAMP) and (TODATE is null or TODATE>CURRENT_TIMESTAMP)")

if arcpy.Exists(finalref):
    arcpy.Delete_management(finalref)
    arcpy.SpatialJoin_analysis('TEMPINT', "FilteredInts", finalref, "JOIN_ONE_TO_MANY", "KEEP_ALL")
else:
    arcpy.SpatialJoin_analysis('TEMPINT', RH_intersections, finalref, "JOIN_ONE_TO_MANY", "KEEP_ALL")
print "DELETING ROWS..."
with arcpy.da.UpdateCursor(finalref, ['ROUTEID','ROUTEID_1','MEASURE','MEASURE_1']) as ucursor:
    for row in ucursor:
        print row
        if row[0] != row[1]:
            ucursor.deleteRow()
        else:
            row[2] = row[3]
            ucursor.updateRow((row))
del ucursor

print "DELETING FIELDS..."
fields = ['Join_Count','TARGET_FID','JOIN_FID','INTERSECTIONID_1','INTERSECTIONNAME_1','ROUTEID_1',
        'FEATURECLASSNAME','FROMDATE','TODATE','MEASURE_1','ATGRADE','created_user','created_date',
        'last_edited_user','last_edited_date','SIGNAL_ID']
for field in fields:
    print field
    arcpy.DeleteField_management(finalref, field)

print 'done'






