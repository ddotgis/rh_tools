#_______________________________________________________________________
#This script updates the Null values in the Block table's BlockID field
#_______________________________________________________________________
import arcpy, os
import os, sys
from pprint import pprint
import requests
import numpy
import operator
import math

scriptloc = sys.path[0]
os.chdir(scriptloc)
gdb = "scratch.gdb"
scratchgdb = os.path.join(scriptloc, gdb)

connection = r"Database Connections\DDOTLRS as LRSVIEWER.sde"
connectiondev = r"Database Connections\DDOTGISDEV as RH user.sde"

routeintfc = os.path.join(connection, 'Ddotlrs.RH.LRSI_RouteIntersection')
rhrdsegtbl = os.path.join(scratchgdb, 'RH_BlockTable')
enhancedjoin = os.path.join(scratchgdb, 'enhancedrtint')
rhrdwyint = os.path.join(scratchgdb, 'RH_BlockIntersections')

whereclause_segs="(FromDate is null or FromDate<=CURRENT_TIMESTAMP) and (ToDate is null or ToDate>CURRENT_TIMESTAMP)"
routeint_layer = "routeint_layer"
rhint_layer = "rhint_layer"
if not arcpy.Exists(routeint_layer):
    arcpy.MakeFeatureLayer_management(routeintfc,routeint_layer,where_clause=whereclause_segs)
else:
    arcpy.Delete_management(routeint_layer)
    arcpy.MakeFeatureLayer_management(routeintfc,routeint_layer,where_clause=whereclause_segs)

if not arcpy.Exists(rhint_layer):
    arcpy.MakeFeatureLayer_management(rhrdwyint,rhint_layer)
else:
    arcpy.Delete_management(rhint_layer)
    arcpy.MakeFeatureLayer_management(rhrdwyint,rhint_layer)

if not arcpy.Exists(enhancedjoin):
    arcpy.SpatialJoin_analysis(routeint_layer, rhint_layer, enhancedjoin)
else:
    arcpy.Delete_management(enhancedjoin)
    arcpy.SpatialJoin_analysis(routeint_layer, rhint_layer, enhancedjoin)

with arcpy.da.SearchCursor(enhancedjoin, ['ROUTEID','Measure','INTERSECTIONID_1'], where_clause="(FROMDATE is null or FROMDATE<=CURRENT_TIMESTAMP) and (TODATE is null or TODATE>CURRENT_TIMESTAMP)") as scursor1:
    intdict = {}
    for row in scursor1:
        if row[0] not in intdict:
            intdict[row[0]] = {}
            intdict[row[0]][row[1]] = row[2]
        else:
            intdict[row[0]][row[1]] = row[2]

#UPDATING TABLE               
with arcpy.da.UpdateCursor(rhrdsegtbl, ['RouteID','BlockID','FromMeasure','ToMeasure','FromIntersectionID', 'ToIntersectionID']) as ucursor:
    for row in ucursor:
        fmeas = float(row[2])
        tmeas = float(row[3])
        rtid = row[0]
        print row[0], row[2], row[3]

        if rtid == '12020342':
            print "OK"
        if rtid == '58024452' or rtid == '58024442':
            continue

        if row[4] == 'null1' or row[4] == 'null2':            
            keylist = list(intdict[rtid].keys())
            finalkey = None
            for key in keylist:
                closekey = abs(fmeas - key)
                if finalkey == None:
                    finalkey = closekey
                    outmeas = key
                elif closekey < finalkey:
                    finalkey = closekey
                    outmeas = key
                else:
                    continue
            fmeas = outmeas
            fromintid = intdict[rtid][fmeas]
            if fromintid == None:
                fromintid = 'None'
            row[4] = fromintid
            
        if row[5] == 'null1' or row[5] == 'null2':
            keylist = list(intdict[rtid].keys())
            finalkey = None
            for key in keylist:
                closekey = abs(tmeas - key)
                if finalkey == None:
                    finalkey = closekey
                    outmeas = key
                elif closekey < finalkey:
                    finalkey = closekey
                    outmeas = key
                else:
                    continue
            tmeas = outmeas
            tointid = intdict[rtid][tmeas]
            if tointid == None:
                tointid = 'None'
            row[5] = tointid
        row[1] = row[0] + '-' + row[4] + '-' + row[5]
        ucursor.updateRow(row)

with arcpy.da.InsertCursor(rhrdsegtbl, ['RouteID','FromMeasure','ToMeasure','FromIntersectionID', 'ToIntersectionID','RouteName', 'BlockID','FromStreet','ToStreet']) as icursor:
        icursor.insertRow(('15048442', 0, 201.437473, 'DCBoundary', 'DCBoundary', 'INTERSTATE 95 BN', '15048442-DCBoundary-DCBoundary','DCBoundary','DCBoundary'))

lastBlockID = 0
with arcpy.da.UpdateCursor(rhrdsegtbl, ['BlockID'], sql_clause=(None, 'ORDER BY BlockID')) as ucursor:
    for row in ucursor:
        thisBlockID = row[0]
        if thisBlockID == lastBlockID:
            lastBlockID = thisBlockID
            ucursor.deleteRow()
            continue
        lastBlockID = thisBlockID
