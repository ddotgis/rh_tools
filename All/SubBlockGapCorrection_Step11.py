#-----------------------------------------------------------------------
#This script takes into account the gaps in the roadways and adjusts the
#SubBlocks accordingly.
#-----------------------------------------------------------------------

import arcpy, os, sys
import requests
import numpy
import operator
import math

def FindDistance(x1, y1, x2, y2):
    distance = int(math.sqrt((x2-x1)**2 + (y2-y1)**2))
    return distance

scriptloc = sys.path[0]
os.chdir(scriptloc)
gdb = "scratch.gdb"
scratchgdb = os.path.join(scriptloc, gdb)

connection = r"Database Connections\DDOTLRS as LRSVIEWER.sde"
connectiondev = r"Database Connections\DDOTGISDEV as RH user.sde"

roadgap = os.path.join(scratchgdb, 'RH_RoadwayGaps')
routeintfc = os.path.join(connection, 'RH.LRSI_RouteIntersection')
rhrdwyint = os.path.join(scratchgdb, 'RH_SubBlockIntersections')
rdwy = os.path.join(connection, 'Ddotlrs.RH.LRSN_Roadway')
enhancedsegtbl = os.path.join(scratchgdb, 'RH_SubBlockTable')
sbblkerror = os.path.join(scratchgdb, 'SubBlockErrorTable')
stsegments = os.path.join(scratchgdb, 'RH_BlockTable')

with arcpy.da.SearchCursor(rdwy, ['ROUTEID','ROUTENAME'], where_clause="(FROMDATE is null or FROMDATE<=CURRENT_TIMESTAMP) and (TODATE is null or TODATE>CURRENT_TIMESTAMP)") as scursor1:
    idtonamedict = {}
    nametoiddict = {}
    for row in scursor1:
        idtonamedict[row[0]] = row[1]
        nametoiddict[row[1]] = row[0]
del scursor1

rtintcoor = {}
rtintdic = {}
with arcpy.da.SearchCursor(routeintfc, ['ROUTEID','MEASURE','SHAPE@XY'],where_clause="(FROMDATE is null or FROMDATE<=CURRENT_TIMESTAMP) and (TODATE is null or TODATE>CURRENT_TIMESTAMP) and (AtGrade is null or AtGrade <> 1)", sql_clause=(None, 'ORDER BY ROUTEID, MEASURE')) as scursor:
    for row in scursor:
        rtid = row[0]
        meas = row[1]

        if rtid not in rtintcoor:
            rtintcoor[rtid] = []
            rtintcoor[rtid].append(row[2])
        else:
            rtintcoor[rtid].append(row[2])


        if rtid not in rtintdic:
            rtintdic[rtid] = {}
            rtintdic[rtid][meas] = row[2]
        else:
            rtintdic[rtid][meas] = row[2]

del scursor

with arcpy.da.SearchCursor(roadgap, ['RouteID','X_Coord','Y_Coord','Measure']) as scursor:
    roadgapdic = {}
    for row in scursor:
        if row[0] not in roadgapdic:
            roadgapdic[row[0]] = {}
            roadgapdic[row[0]][row[3]] = (row[1],row[2])
        else:
            if row[3] in roadgapdic[row[0]]:
                roadgapdic[row[0]][row[3] + .001] = (row[1],row[2])
            else:
                roadgapdic[row[0]][row[3]] = (row[1],row[2])
del scursor

print "UPDATING TABLE"
#UPDATING TABLE
insertlist = []
skipvar = 0
with arcpy.da.UpdateCursor(enhancedsegtbl, ['RouteID','FromMeasure','ToMeasure','FromIntersectionID', 'ToIntersectionID','RouteName','SubBlockID','RoadType','BlockID','FromSubBlock','ToSubBlock'], sql_clause=(None, 'ORDER BY RouteID, FromMeasure')) as ucursor:
    for row in ucursor:
        rtid = row[0]
        fmeas = row[1]
        tmeas = row[2]
        fintid = row[3]
        tintid = row[4]
        rtname = row[5]
        blockid = row[6]
        rdtype = row[7]

        #CHECK THIS ONE IN THE FUTURE VVV
        if rtid == '11003702' and fmeas > 310:
            continue
        if 'DCBoundary' in fintid:
            continue
        if rtid == '47084822':
            continue
        if row[8] == 'ERROR':
            row[8] = rtid + '-' + fintid + '-' + tintid
            ucursor.updateRow((row))

        print rtid, fmeas
        
        if fintid != 'Dead End' and fintid != 'None':
            fintidlist = fintid.split('_')
            frtnamelist = []
            for rte in fintidlist:
                if '*' in rte:
                    rte = rte[:-2]
                if rdtype == 1:
                    frtnamelist.append(idtonamedict[rte])
                else:
                    frtnamelist.append(rte)
            for rte in frtnamelist:
                if rte == rtname:
                    frtnamelist.remove(rte)
            fnamestr = ''
            for rte in frtnamelist:
                if fnamestr == '':
                    fnamestr += str(rte)
                else:
                    fnamestr += '/%s' % rte
            row[9] = fnamestr

        elif fintid == 'None':
            row[9] = 'None'
        else:
            row[9] = 'Dead End'

        if tintid != 'Dead End' and tintid != 'None':
            tintidlist = tintid.split('_')
            trtnamelist = []
            for rte in tintidlist:
                if '*' in rte:
                    rte = rte[:-2]
                if rdtype == 1:
                    trtnamelist.append(idtonamedict[rte])
                else:
                    trtnamelist.append(rte)
            for rte in trtnamelist:
                if rte == rtname:
                    trtnamelist.remove(rte)
            tnamestr = ''
            for rte in trtnamelist:
                if tnamestr == '':
                    tnamestr += str(rte)
                else:
                    tnamestr += '/%s' % rte
            row[10] = tnamestr
        elif tintid == 'None':
            row[10] = 'None'
        else:
            row[10] = 'Dead End'

        ucursor.updateRow((row))


            
        if rtid in roadgapdic:
            gapmeaslist = []
            for meas in roadgapdic[rtid]:
                if meas > fmeas and meas < tmeas:
                    gapmeaslist.append(meas)
                else:
                    continue

            if len(gapmeaslist) == 1:
                if (gapmeaslist[0] < (tmeas + .2) and gapmeaslist[0] > (tmeas - .2)):
                    rdgaplist = roadgapdic[rtid].keys()
                    rdgaplist.sort()
                    for meas in rdgaplist:
                        if meas > gapmeaslist[0]:
                            gapmeaslist.append(meas)
                            break
                elif (gapmeaslist[0] < (fmeas + .2) and gapmeaslist[0] > (fmeas - .2)):
                    xsort = sorted(roadgapdic[rtid], key=roadgapdic[rtid].get)
                    xsort.sort()
                    for meas in xsort:
                        thisMeas = meas
                        if thisMeas == gapmeaslist[0]:
                            gapmeaslist.append(lastMeas)
                            break
                        lastMeas = thisMeas

            if len(gapmeaslist) > 0:
#                if rdtype == 1:
                if (gapmeaslist[0] < (tmeas + .2) and gapmeaslist[0] > (tmeas - .2)) and (gapmeaslist[0] < (fmeas + .2) and gapmeaslist[0] > (fmeas - .2)):
                    continue

                if gapmeaslist[0] < (tmeas + .2) and gapmeaslist[0] > (tmeas - .2):
                    gapmeaslist.sort()

                    for intmeas in rtintdic[rtid]:
                        if (gapmeaslist[0] < intmeas + .2) and (gapmeaslist[0] > intmeas - .2):
                            if rtintdic[rtid][intmeas] != roadgapdic[rtid].values()[0]:
                                skipvar = 1
                    if skipvar == 1:
                        skipvar = 0
                        continue
                    gapcoord = roadgapdic[rtid][gapmeaslist[0]]

                    proxi = 0
                    for c in rtintcoor[rtid]:
                        dis = FindDistance(c[0],c[1],gapcoord[0],gapcoord[1])
                        if dis <= 2:
                            proxi = 1
                            break
                    if proxi != 1:
                        row[4] = 'Dead End'
                        row[6] = rtid + '-' + fintid + '-Dead End'
                        row[8] = rtid + '-' + fintid + '-Dead End'
                    row[2] = gapmeaslist[0]

                    ucursor.updateRow((row))

                elif gapmeaslist[0] < (fmeas + .2) and gapmeaslist[0] > (fmeas - .2):
                    gapmeaslist.sort()

                    for intmeas in rtintdic[rtid]:
                        if (gapmeaslist[1] < intmeas + .2) and (gapmeaslist[1] > intmeas - .2):
                            if rtintdic[rtid][intmeas] != roadgapdic[rtid].values()[1]:
                                skipvar = 1
                    if skipvar == 1:
                        skipvar = 0
                        continue
                    gapcoord = roadgapdic[rtid][gapmeaslist[1]]
                    

                    proxi = 0
                    for c in rtintcoor[rtid]:
                        dis = FindDistance(c[0],c[1],gapcoord[0],gapcoord[1])
                        if dis <= 2:
                            proxi = 1
                            break
                    if proxi != 1:
                        row[3] = 'Dead End'
                        row[6] = rtid + '-Dead End-' + tintid
                        row[8] = rtid + '-Dead End-' + tintid
                    row[1] = gapmeaslist[1]

                    ucursor.updateRow((row))

                else:
                    gapmeaslist.sort()
                    if len(gapmeaslist) == 4:
                        newgapmeaslist = []
                        for meas in gapmeaslist:
                            if (roadgapdic[rtid][meas] not in rtintdic[rtid].values()) and ((meas > fmeas + .2) and (meas < tmeas - .2)):
                                newgapmeaslist.append(meas)
                        newgapmeaslist.sort()
                    else:
                        newgapmeaslist = gapmeaslist

                    sblockid = rtid + '-Dead End' + '-' + tintid

                    insertlist.append((rtid,newgapmeaslist[1],tmeas,'Dead End',tintid,rtname,sblockid,rdtype,sblockid))

                    row[2] = newgapmeaslist[0]
                    row[4] = 'Dead End'
                    row[6] = rtid + '-' + fintid + '-Dead End'
                    row[8] = rtid + '-' + fintid + '-Dead End'

                    ucursor.updateRow((row))
            else:
                continue

print "INSERTING NEW ROWS"
with arcpy.da.InsertCursor(enhancedsegtbl, ['RouteID','FromMeasure','ToMeasure','FromIntersectionID', 'ToIntersectionID','RouteName','SubBlockID','RoadType','BlockID']) as icursor:
    for item in insertlist:
        print item
        icursor.insertRow((item))

del icursor
print "DONE"




























