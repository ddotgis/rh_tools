#-------------------------------------------------------------------------------------------------
#This script creates new single point SubBlock intersections, as opposed to stacked intersections.
#-------------------------------------------------------------------------------------------------
import arcpy, os, sys
import requests
import numpy
import operator
import math
import hashlib

def Table2Dict(inputFeature,key_field,field_names):
    try:
        # type: (feature class, unique id, list of field names) -> nested dictionary
        """Convert table to nested dictionary.

        The key_field -- feature ID for input feature class
        field_names -- selected field names for input feature class
        return outDict={key_field:{field_name1:value1,field_name2:value2...}}
        :rtype: nested ditionary
        """
        outDict={}
        field_names.insert(0,key_field)
        with arcpy.da.SearchCursor(inputFeature,field_names) as cursor:
            for row in cursor:
                outDict[row[0]]=dict(zip(field_names[1:],row[1:]))
        return outDict
    except Exception as e:
        return 'Error occured : ' + str(e)


scriptloc = sys.path[0]
gdb = "scratch.gdb"
scratchgdb = os.path.join(scriptloc, gdb)

connection = r"Database Connections\DDOTLRS as LRSVIEWER.sde"
connection2 = r"Database Connections\DDOTLRS as RH user.sde\Ddotlrs.RH.ReferenceFeatures"

sbint_write = os.path.join(connection2, 'Ddotlrs.RH.SubBlockIntersection')
RH_roadway = os.path.join(connection, "Ddotlrs.RH.LRSN_Roadway")
RH_intersections = os.path.join(connection, "Ddotlrs.RH.LRSI_RouteIntersection")
RH_intersections_local = os.path.join(scratchgdb, 'RouteIntersections_local')
sblockintFC = os.path.join(scratchgdb, 'RH_SubBlockIntersections')

if not arcpy.Exists(scratchgdb):
    arcpy.CreateFileGDB_management(scriptloc, os.path.basename(scratchgdb))

if arcpy.Exists(RH_intersections_local):
    arcpy.Delete_management(RH_intersections_local)

if not arcpy.Exists(sblockintFC):
    spatial_reference = arcpy.Describe(RH_intersections).spatialReference
    arcpy.CreateFeatureclass_management(scratchgdb, os.path.basename(sblockintFC), "POINT", spatial_reference=spatial_reference)
    arcpy.AddField_management(sblockintFC, "INTERSECTIONID", "TEXT", 500, "", "", "", "NULLABLE", "REQUIRED")
    arcpy.AddField_management(sblockintFC, "ROUTEID", "TEXT", 500, "", "", "", "NULLABLE", "REQUIRED")
    arcpy.AddField_management(sblockintFC, "MEASURE", "FLOAT", '12', "3", "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(sblockintFC, "INTERSECTIONNAME", "TEXT", 500, "", "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(sblockintFC, "GRADE", "SHORT", '', "", "", "", "NULLABLE", "NON_REQUIRED",field_domain='AtGradeInt')
    arcpy.AddField_management(sblockintFC, "INTERSECTIONKEY", "TEXT", 100, "", "", "", "NULLABLE", "REQUIRED")
    
else:
    arcpy.TruncateTable_management(sblockintFC)
#DDOT has added a 'Grade' field and populated it with information ourselves


arcpy.CopyFeatures_management(RH_intersections,RH_intersections_local)

roadway_dict = Table2Dict(RH_roadway,"RouteId",["RoadType", "RouteName"])
roadtype_dict =[d.codedValues for d in arcpy.da.ListDomains(connection) if d.name == 'RoadType'][0]

#CREATING ROADWAY DICTIONARY
rdict = dict()
sdict = dict()
edict = dict()

with arcpy.da.SearchCursor(RH_roadway, ['ROUTEID','RouteName','RoadType','SHAPE@'], where_clause="(FromDate is null or FromDate<=CURRENT_TIMESTAMP) and (ToDate is null or ToDate>CURRENT_TIMESTAMP)") as cursor:
    for row in cursor:
        routeid = str(row[0])
        startpt = math.floor(row[3].firstPoint.X),math.floor(row[3].firstPoint.Y)
        endpt = math.floor(row[3].lastPoint.X),math.floor(row[3].lastPoint.Y)
        rdict[routeid] = [startpt,endpt]
        sdict[startpt] = [routeid,row[2],endpt]
        edict[endpt] = [routeid, row[2], startpt]        
del cursor


newintDict = dict()  # dict to store intersection data.  key=routeid, value is {streetname1,streetname2,atgrade or grade-separated, x,y}
rtidDict = dict()
#CREATING INT DICTIONARY
with arcpy.da.SearchCursor(RH_intersections, ['ROUTEID','Measure','INTERSECTIONNAME','AtGrade','SHAPE@XY','FeatureID','SHAPE@X', 'SHAPE@Y'],where_clause="(FROMDATE is null or FROMDATE<=CURRENT_TIMESTAMP) and (TODATE is null or TODATE>CURRENT_TIMESTAMP) and (ATGRADE is null or ATGRADE <> 1)") as cursor:

    print "Creating intersection dictionary..."
    for row in cursor:
        roadwaytype = roadtype_dict[roadway_dict[row[0]]['RoadType']]
        crossingroadwaytype = roadtype_dict[roadway_dict[row[5]]['RoadType']]
        roadwayname = roadway_dict[row[0]]['RouteName']
        crossingroadwayname = roadway_dict[row[5]]['RouteName']
        ramptype = 'N/A'
        coorconv = '(' + str(math.floor(row[6])) + ', ' + str(math.floor(row[7])) + ')'
        
        #if roadwaytype.lower() in ['driveway','alley'] or crossingroadwaytype.lower() in ['driveway','alley']:
            #exclude any intersection that connects to routes we don't care about.
            #continue
        if roadwaytype.lower() == 'service road':
            #add type info
            roadwayname += ' Service Rd'
        if crossingroadwaytype.lower() == 'service road':
            #add type info
            crossingroadwayname += ' Service Rd'


        if str(roadwaytype) == 'Street' and str(crossingroadwaytype) == 'Ramp' and 'interstate' in roadwayname.lower() and row[3] == 0:
            if coorconv == str(rdict[row[5]][0]):
                ramptype = 'EXIT'
            if coorconv == str(rdict[row[5]][1]):
                ramptype = 'ENTRANCE'

        if str(roadwaytype) == 'Ramp' and str(crossingroadwaytype) == 'Street' and 'interstate' in crossingroadwayname.lower() and row[3] == 0:
            if coorconv == str(rdict[row[0]][0]):
                ramptype = 'EXIT'
            if coorconv == str(rdict[row[0]][1]):
                ramptype = 'ENTRANCE'        

        rtid = row[0]
        print "working route ID: " + str(rtid)
        measure = round(row[1],3)
        intnm = row[2]
        atgr = row[3]
        xcoor = row[4][0]
        ycoor = row[4][1]
        xycoor = row[4]
        
        if intnm != None:
            st1 = roadwayname
            st2 = crossingroadwayname
            #we care about all streets, ramps or service roads
            if rtid in newintDict:
                if measure in newintDict[rtid]:
                    newintDict[rtid][measure][0] = str(newintDict[rtid][measure][0]) + ' & ' + str(st2)
                else:
                    newintDict[rtid][measure] = ['%s & %s' % (st1, st2), atgr, xcoor, ycoor, ramptype]
            else:
                newintDict[rtid] = {}
                newintDict[rtid][measure] = ['%s & %s' % (st1, st2), atgr, xcoor, ycoor, ramptype]

        if row[4] not in rtidDict:
            rtidDict[xycoor] = str(rtid)
        else:
            if rtid not in rtidDict[xycoor]:
                rtidDict[xycoor] += '_%s' % (rtid)

    result = {}
    for key,value in rtidDict.items():
        if value not in result.values():
            result[key] = value
        elif value in result.values():
            if value + '*1' in result.values():
                if value + '*2' in result.values():
                    if value + '*3' in result.values():
                        print '...'
                    else:
                       result[key] = value + '*3' 
                else:
                    result[key] = value + '*2'
            else:
                result[key] = value + '*1'

del cursor
                 
#EDITS DICTIONARY TO REMOVE OVERLAYING INTERSECTIONS

#Make a X_Y key for every intersection and see if we get the same thing
xydict = dict()
for routeids,events in newintDict.items():
    for event, intersection in events.items():
        newkey = str(math.floor(intersection[2])) + "_" + str(math.floor(intersection[3]))
        intersection.append(routeids)
        intersection.append(event)
        xydict[newkey] = intersection

#UPDATING ATGRADE INFO
print "Updating table..."
with arcpy.da.InsertCursor(sblockintFC, ['INTERSECTIONID','ROUTEID','MEASURE','INTERSECTIONNAME','GRADE','SHAPE@X','SHAPE@Y','INTERSECTIONKEY']) as icursor:   
    count = 0
    for k,v in xydict.items():               
        count = count + 1
        blockhas = hashlib.md5(result[v[2],v[3]])
        intkey = str(blockhas.hexdigest())
        print count
        icursor.insertRow((result[v[2],v[3]], v[5], v[6], v[0], v[1], v[2], v[3],intkey))

del icursor


# with arcpy.da.UpdateCursor(sblockintFC, ['INTERSECTIONID','ROUTEID','MEASURE','INTERSECTIONNAME','GRADE','SHAPE@X','SHAPE@Y', 'SHAPE@XY', 'OID@'], where_clause="(GRADE is null or GRADE <> 1)") as cursor:

#     #all at-grade intersections only
#     for row in cursor:
#         if row[8] == 3613:
#             print 'STOP'
#         print row
#         if 'ramp-' in row[3].lower() and 'interstate' in row[3].lower():
#         # working with ramp intersections only
#             coorid = str(math.floor(row[5])) + "_" + str(math.floor(row[6]))
#             ramptype = str(xydict[coorid][4])
#             begstreets = row[3].split(' & ')
#             for street in begstreets:
#                 if 'ramp' in street.lower():
#                     continue
#                 begstreet = str(street)
                
#             try:
#                 if ramptype == 'EXIT':
#                     nextintcoor = sdict[(math.floor(row[7][0]),math.floor(row[7][1]))][2]
#                     nextintid = str(round(nextintcoor[0],2)) + "_" + str(round(nextintcoor[1],2))
#                     if nextintid in xydict:
#                         if str(xydict[nextintid][0].lower()).count("ramp") >= 2:
#                             nextintcoor = sdict[sdict[(math.floor(row[7][0]),math.floor(row[7][1]))][2]][2]
#                             nextintid = str(round(nextintcoor[0],2)) + "_" + str(round(nextintcoor[1],2))
#                             if nextintid in xydict:
#                                 if str(xydict[nextintid][0].lower()).count("ramp") >= 2:
#                                     nextintcoor = sdict[sdict[sdict[(math.floor(row[7][0]),math.floor(row[7][1]))][2]][2]][2]
#                                     nextintid = str(round(nextintcoor[0],2)) + "_" + str(round(nextintcoor[1],2))
#                                 else:
#                                     finintid = nextintid
#                                     finstreets = str(xydict[finintid][0]).split(' & ')
#                                     for street in finstreets:
#                                         if 'ramp' in street.lower():
#                                             continue
#                                         desstreet = str(street)
#                                     row[3] = '%s ramp from %s to %s' % (ramptype, begstreet, desstreet)
#                             else:
#                                 row[3] = '%s ramp from %s to NOWHERE' % (ramptype, begstreet)
#                         else:
#                             finintid = nextintid
#                             finstreets = str(xydict[finintid][0]).split(' & ')
#                             for street in finstreets:
#                                 if 'ramp' in street.lower():
#                                     continue
#                                 desstreet = street
#                             row[3] = '%s ramp from %s to %s' % (ramptype, begstreet, desstreet)
#                     else:
#                         row[3] = '%s ramp from %s to NOWHERE' % (ramptype, begstreet)


#                 if ramptype == 'ENTRANCE':
#                     nextintcoor = edict[(math.floor(row[7][0]),math.floor(row[7][1]))][2]
#                     nextintid = str(round(nextintcoor[0],2)) + "_" + str(round(nextintcoor[1],2))
#                     if nextintid in xydict:
#                         if str(xydict[nextintid][0].lower()).count("ramp") >= 2:
#                             nextintcoor = edict[edict[(math.floor(row[7][0]),math.floor(row[7][1]))][2]][2]
#                             nextintid = str(round(nextintcoor[0],2)) + "_" + str(round(nextintcoor[1],2))
#                             if nextintid in xydict:
#                                     if str(xydict[nextintid][0].lower()).count("ramp") >= 2:
#                                         nextintcoor = edict[edict[edict[(math.floor(row[7][0]),math.floor(row[7][1]))][2]][2]][2]
#                                         nextintid = str(round(nextintcoor[0],2)) + "_" + str(round(nextintcoor[1],2))

#                                     else:
#                                         finintid = nextintid
#                                         finstreets = str(xydict[finintid][0]).split(' & ')
#                                         for street in finstreets:
#                                             if 'ramp' in street.lower():
#                                                 continue
#                                             desstreet = str(street)
#                                         row[3] = '%s ramp from %s to %s' % (ramptype, desstreet, begstreet)

#                             else:
#                                 row[3] = '%s ramp from NOWHERE to %s' % (ramptype, begstreet)
#                         else:
#                             finintid = nextintid
#                             finstreets = str(xydict[finintid][0]).split(' & ')
#                             for street in finstreets:
#                                 if 'ramp' in street.lower():
#                                     continue
#                                 desstreet = street
#                             row[3] = '%s ramp from %s to %s' % (ramptype, desstreet, begstreet)
#                     else:
#                         row[3] = '%s ramp from NOWHERE to %s' % (ramptype, begstreet)
#             except KeyError:
#                 row[3] = 'ERROR'
                
#                     #NAME OF DESTINATION STREET ^^^
        
#         #cursor.updateRow(row)

# del cursor

#SORTING INTERSECTION IDs               
with arcpy.da.UpdateCursor(sblockintFC, ['INTERSECTIONID']) as ucursor:

    for row in ucursor:
        intid = row[0]
        print intid

        intparts = intid.split('_')
        intparts.sort()

        intparts = '_'.join(intparts)
        
        row[0] = intparts

        ucursor.updateRow(row)

del ucursor

print "Deleting duplicate rows..."
arcpy.DeleteIdentical_management(sblockintFC, ['ROUTEID','MEASURE','INTERSECTIONNAME','GRADE'])

print 'Clearing out previous subblock int data...'
arcpy.TruncateTable_management(sbint_write)

print 'Appending updated subblock int data to SDE...'
arcpy.Append_management(sblockintFC, sbint_write, "NO_TEST")


print 'done'






