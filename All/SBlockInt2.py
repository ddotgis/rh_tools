#-------------------------------------------------------------------------------------------------
#This script creates new single point SubBlock intersections, as opposed to stacked intersections.
#-------------------------------------------------------------------------------------------------
import arcpy, os, sys
import requests
import numpy
import operator
import math
import hashlib

def Table2Dict(inputFeature,key_field,field_names):
    try:
        # type: (feature class, unique id, list of field names) -> nested dictionary
        """Convert table to nested dictionary.

        The key_field -- feature ID for input feature class
        field_names -- selected field names for input feature class
        return outDict={key_field:{field_name1:value1,field_name2:value2...}}
        :rtype: nested ditionary
        """
        outDict={}
        field_names.insert(0,key_field)
        with arcpy.da.SearchCursor(inputFeature,field_names) as cursor:
            for row in cursor:
                outDict[row[0]]=dict(zip(field_names[1:],row[1:]))
        return outDict
    except Exception as e:
        return 'Error occured : ' + str(e)


scriptloc = sys.path[0]
gdb = "scratch.gdb"
scratchgdb = os.path.join(scriptloc, gdb)

connection = r"Database Connections\DDOTLRS as LRSVIEWER.sde"
connection2 = r"Database Connections\DDOTLRS as RH user.sde\Ddotlrs.RH.ReferenceFeatures"

sbint_write = os.path.join(connection2, 'Ddotlrs.RH.SubBlockIntersection')
RH_roadway = os.path.join(connection, "Ddotlrs.RH.LRSN_Roadway")
RH_intersections = os.path.join(connection, "Ddotlrs.RH.LRSI_RouteIntersection")
RH_intersections_local = os.path.join(scratchgdb, 'RouteIntersections_local')
sblockintFC = os.path.join(scratchgdb, 'RH_SubBlockIntersections2')
intref = os.path.join(scratchgdb, 'RH_IntReference2')
rtintrdwyD = os.path.join(scratchgdb, 'RouteIntRdwy2')
rtintrdwy = os.path.join(scratchgdb, 'RouteIntRdwy3')

if not arcpy.Exists(scratchgdb):
    arcpy.CreateFileGDB_management(scriptloc, os.path.basename(scratchgdb))

if arcpy.Exists(RH_intersections_local):
    arcpy.Delete_management(RH_intersections_local)

arcpy.MakeFeatureLayer_management(RH_intersections, 'RouteInts',where_clause="(FROMDATE is null or FROMDATE<=CURRENT_TIMESTAMP) and (TODATE is null or TODATE>CURRENT_TIMESTAMP) and (AtGrade is null or AtGrade <> 1)")
arcpy.MakeFeatureLayer_management(RH_roadway, 'RoadwayLyr',where_clause="(FROMDATE is null or FROMDATE<=CURRENT_TIMESTAMP) and (TODATE is null or TODATE>CURRENT_TIMESTAMP)")


if arcpy.Exists(rtintrdwy) == False:
    print "RE-ROUTING ROUTEINTERSECTIONS"
    arcpy.LocateFeaturesAlongRoutes_lr('RouteInts', 'RoadwayLyr', 'ROUTEID', "2 Meters", rtintrdwy, "RID POINT MEAS",route_locations="ALL")


#with arcpy.da.UpdateCursor(rtintrdwy, ['FEATUREID','RID'],where_clause="FEATUREID <> RID") as ucursor:
#    for row in ucursor:
#        print ("DELETING ROW" + str(row))
#        ucursor.deleteRow()
#del ucursor

RouteFeatIntDict = {}
with arcpy.da.SearchCursor(rtintrdwyD, ['ROUTEID','FEATUREID','MEASURE']) as scursor:
    for row in scursor:
        rtid = row[0]
        ftid = row[1]
        meas = row[2]

        rtjoin = str(rtid) + '_' + str(ftid)
        if rtjoin not in RouteFeatIntDict:
            RouteFeatIntDict[rtjoin] = []
            RouteFeatIntDict[rtjoin].append(round(meas,3))
        else:
            RouteFeatIntDict[rtjoin].append(round(meas,3))
del scursor

RouteMeasIntDict = {}
with arcpy.da.SearchCursor(rtintrdwyD, ['ROUTEID','FEATUREID','MEASURE','MEAS']) as scursor:
    for row in scursor:
        rtid = row[0]
        ftid = row[1]
        meas = row[2]


        if rtid not in RouteMeasIntDict:
            RouteMeasIntDict[rtid] = {}
            RouteMeasIntDict[rtid][round(meas,3)] = {}
            RouteMeasIntDict[rtid][round(meas,3)][ftid] = round(row[3],3)
        else:
            if round(meas,3) not in RouteMeasIntDict[rtid]:
                RouteMeasIntDict[rtid][round(meas,3)] = {}
                RouteMeasIntDict[rtid][round(meas,3)][ftid] = round(row[3],3)
            else:
                RouteMeasIntDict[rtid][round(meas,3)][ftid] = round(row[3],3)        
del scursor

roadway_dict = Table2Dict(RH_roadway,"RouteId",["RouteName"])

if 2==2:
    print "OK"

if not arcpy.Exists(sblockintFC):
    arcpy.CreateTable_management(scratchgdb, os.path.basename(sblockintFC))
    arcpy.AddField_management(sblockintFC, "INTERSECTIONID", "TEXT", 500, "", "", "", "NULLABLE", "REQUIRED")
    arcpy.AddField_management(sblockintFC, "ROUTEID", "TEXT", 500, "", "", "", "NULLABLE", "REQUIRED")
    arcpy.AddField_management(sblockintFC, "MEASURE", "FLOAT", '12', "3", "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(sblockintFC, "INTERSECTIONNAME", "TEXT", 500, "", "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(sblockintFC, "GRADE", "SHORT", '', "", "", "", "NULLABLE", "NON_REQUIRED",field_domain='AtGradeInt')
    arcpy.AddField_management(sblockintFC, "INTERSECTIONKEY", "TEXT", 100, "", "", "", "NULLABLE", "REQUIRED")    
else:
    arcpy.TruncateTable_management(sblockintFC)
#DDOT has added a 'Grade' field and populated it with information ourselves

if not arcpy.Exists(intref):
    arcpy.CreateTable_management(scratchgdb, os.path.basename(intref))
    arcpy.AddField_management(intref, "INTERSECTIONID", "TEXT", 500, "", "", "", "NULLABLE", "REQUIRED")
    arcpy.AddField_management(intref, "ROUTEID", "TEXT", 500, "", "", "", "NULLABLE", "REQUIRED")
    arcpy.AddField_management(intref, "MEASURE", "FLOAT", '12', "3", "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(intref, "INTERSECTIONNAME", "TEXT", 500, "", "", "", "NULLABLE", "NON_REQUIRED")
    arcpy.AddField_management(intref, "GRADE", "SHORT", '', "", "", "", "NULLABLE", "NON_REQUIRED",field_domain='AtGradeInt')
    arcpy.AddField_management(intref, "INTERSECTIONKEY", "TEXT", 100, "", "", "", "NULLABLE", "REQUIRED")
    arcpy.AddField_management(intref, "X_Coord", "DOUBLE", "", "", "", "", "NULLABLE", "REQUIRED")
    arcpy.AddField_management(intref, "Y_Coord", "DOUBLE", "", "", "", "", "NULLABLE", "REQUIRED")  
else:
    arcpy.TruncateTable_management(intref)

icursor = arcpy.da.InsertCursor(sblockintFC, ["INTERSECTIONID","ROUTEID","MEASURE","INTERSECTIONNAME","GRADE","INTERSECTIONKEY"])

insertedList = []
insertedDict = {}
lastMeas = 0
lastRtid = 0
with arcpy.da.SearchCursor(rtintrdwy, ['ROUTEID','FEATUREID','MEASURE','INTERSECTIONNAME','ATGRADE','MEAS'],
                            sql_clause=(None, 'ORDER BY ROUTEID, MEASURE')) as scursor:
    for row in scursor:
        thisRtid = row[0]
        ftid = row[1]
        thisMeas = round(row[2],3)
        intname = row[3]
        atgrade = row[4]
        featmeas = round(row[5],3)
        print row

        if thisRtid in insertedDict and thisMeas in insertedDict[thisRtid]:
            continue

        if thisRtid == '11074162' and thisMeas > 369:
            print "OK"

        if thisMeas not in RouteMeasIntDict[thisRtid]:
            continue
        rtftkey = thisRtid + "_" + ftid
        ftrtkey = ftid + "_" + thisRtid

        crossingroutes = list(RouteMeasIntDict[thisRtid][thisMeas].keys())
        allroutes = list(RouteMeasIntDict[thisRtid][thisMeas].keys())
        allroutes.append(thisRtid)
        allroutes.sort()
        allroutenames = []

        if len(RouteFeatIntDict[rtftkey]) == 1:
            for rte in allroutes:
                allroutenames.append(roadway_dict[rte]["RouteName"])
            allroutenames.sort()

            newIntID ="_".join(allroutes)

            intname = " & ".join(allroutenames)
            inthas = hashlib.md5(newIntID)
            intkey = str(inthas.hexdigest())
            icursor.insertRow((newIntID, thisRtid, thisMeas, intname, atgrade, intkey))

            for rte, m in RouteMeasIntDict[thisRtid][thisMeas].items():
                if rte not in insertedDict:
                    insertedDict[rte] = []
                    insertedDict[rte].append(m)
                else:
                    insertedDict[rte].append(m)
        else:
            dupMeas = RouteFeatIntDict[rtftkey]
            dupMeasList = list(set(dupMeas))
            count = 0
            for dup in dupMeasList:
                
                for rte in allroutes:
                    allroutenames.append(roadway_dict[rte]["RouteName"])
                allroutenames.sort()

                if count == 0:
                    newIntID ="_".join(allroutes)
                else:
                    tempIntID ="_".join(allroutes)
                    newIntID += '_%s' % str(count)

                intname = " & ".join(allroutenames)
                inthas = hashlib.md5(newIntID)
                intkey = str(inthas.hexdigest())
                icursor.insertRow((newIntID, thisRtid, dup, intname, atgrade, intkey))

                for rte, m in RouteMeasIntDict[thisRtid][dup].items():
                    if rte not in insertedDict:
                        insertedDict[rte] = []
                        insertedDict[rte].append(m)
                    else:
                        insertedDict[rte].append(m)
                count += 1            

del icursor
del scursor

        #if len(allroutes) == 2:
        #    for m in RouteFeatIntDict[ftrtkey]:
        #        xroutes = RouteMeasIntDict[ftid][m]
        #        aroutes = xroutes.append(ftid).sort()
        #        if aroutes != allroutes:
        #            continue


