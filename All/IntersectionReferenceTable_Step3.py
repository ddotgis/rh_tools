
import arcpy, os
import os, sys
from pprint import pprint
import requests
import numpy
import operator
import math

def Table2Dict(inputFeature,key_field,field_names):
    # type: (feature class, unique id, list of field names) -> nested dictionary
    """Convert table to nested dictionary.

    The key_field -- feature ID for input feature class
    field_names -- selected field names for input feature class
    return outDict={key_field:{field_name1:value1,field_name2:value2...}}
    :rtype: nested ditionary
    """
    outDict={}
    field_names.insert(0,key_field)
    with arcpy.da.SearchCursor(inputFeature,field_names) as cursor:
        for row in cursor:
            outDict[row[0]]=dict(zip(field_names[1:],row[1:]))
    return outDict


scriptloc = sys.path[0]
gdb = "scratch.gdb"
scratchgdb = os.path.join(scriptloc, gdb)

connection = r"Database Connections\DDOTLRS as LRSVIEWER.sde"
connection2 = r"Database Connections\DDOTLRS as RH user.sde"

RH_roadway = os.path.join(connection, "Ddotlrs.RH.LRSN_Roadway")
RH_intersections = os.path.join(connection, "Ddotlrs.RH.LRSI_RouteIntersection")
RH_intersections_local = os.path.join(scratchgdb, 'RouteIntersections_local')
enhancedintersection = os.path.join(connection, "Ddotlrs.RH.EnhancedIntersection")
newfc = os.path.join(scratchgdb, 'RH_IntersectionReference')

if not arcpy.Exists(scratchgdb):
    arcpy.CreateFileGDB_management(scriptloc, os.path.basename(scratchgdb))

if arcpy.Exists(RH_intersections_local):
    arcpy.Delete_management(RH_intersections_local)

    

if not arcpy.Exists(newfc):
    arcpy.CreateTable_management(scratchgdb,os.path.basename(newfc))
    arcpy.AddField_management(newfc, "NEWINTERSECTIONID", "TEXT", 500, "", "", "", "NULLABLE", "REQUIRED")
    arcpy.AddField_management(newfc, "OLDINTERSECTIONID", "TEXT", 500, "", "", "", "NULLABLE", "REQUIRED")
    arcpy.AddField_management(newfc, "ROUTEID", "TEXT", 50)
    arcpy.AddField_management(newfc, "MEASURE", "FLOAT", 50)
    
else:
    arcpy.TruncateTable_management(newfc)

arcpy.CopyFeatures_management(RH_intersections,RH_intersections_local)

roadway_dict = Table2Dict(RH_roadway,"RouteId",["RoadType", "RouteName"])
roadtype_dict =[d.codedValues for d in arcpy.da.ListDomains(connection) if d.name == 'RoadType'][0]

#CREATING ROADWAY DICTIONARY
rdict = dict()
sdict = dict()
edict = dict()
riddict = dict()
with arcpy.da.SearchCursor(RH_roadway, ['ROUTEID','RouteName','RoadType','SHAPE@'], where_clause="(FromDate is null or FromDate<=CURRENT_TIMESTAMP) and (ToDate is null or ToDate>CURRENT_TIMESTAMP)") as cursor:
    for row in cursor:
        routeid = str(row[0])
        startpt = math.floor(row[3].firstPoint.X),math.floor(row[3].firstPoint.Y)
        endpt = math.floor(row[3].lastPoint.X),math.floor(row[3].lastPoint.Y)
        rdict[routeid] = [startpt,endpt]
        sdict[startpt] = [routeid,row[2],endpt]
        edict[endpt] = [routeid, row[2], startpt]
        riddict[row[1]] = [routeid]
        
del cursor


d = dict()  # dict to store intersection data.  key=routeid, value is {streetname1,streetname2,atgrade or grade-separated, x,y}
rtiddict = dict()
#CREATING INT DICTIONARY
with arcpy.da.SearchCursor(RH_intersections, ['ROUTEID','Measure','INTERSECTIONNAME','AtGrade','SHAPE@XY','FeatureID','SHAPE@X', 'SHAPE@Y','INTERSECTIONID'],where_clause="(FROMDATE is null or FROMDATE<=CURRENT_TIMESTAMP) and (TODATE is null or TODATE>CURRENT_TIMESTAMP) and (ATGRADE is null or ATGRADE <> 1)") as cursor:

    print "Creating intersection dictionary..."
    for row in cursor:
        roadwaytype = roadtype_dict[roadway_dict[row[0]]['RoadType']]
        crossingroadwaytype = roadtype_dict[roadway_dict[row[5]]['RoadType']]
        roadwayname = roadway_dict[row[0]]['RouteName']
        crossingroadwayname = roadway_dict[row[5]]['RouteName']
        ramptype = 'N/A'
        coorconv = '(' + str(math.floor(row[6])) + ', ' + str(math.floor(row[7])) + ')'
        
        if roadwaytype.lower() in ['driveway','alley'] or crossingroadwaytype.lower() in ['driveway','alley']:
            #exclude any intersection that connects to routes we don't care about.
            continue
        if roadwaytype.lower() == 'service road':
            #add type info
            roadwayname += ' Service Rd'
        if crossingroadwaytype.lower() == 'service road':
            #add type info
            crossingroadwayname += ' Service Rd'


        if str(roadwaytype) == 'Street' and str(crossingroadwaytype) == 'Ramp' and 'interstate' in roadwayname.lower() and row[3] == 0:
            if coorconv == str(rdict[row[5]][0]):
                ramptype = 'EXIT'
            if coorconv == str(rdict[row[5]][1]):
                ramptype = 'ENTRANCE'

        if str(roadwaytype) == 'Ramp' and str(crossingroadwaytype) == 'Street' and 'interstate' in crossingroadwayname.lower() and row[3] == 0:
            if coorconv == str(rdict[row[0]][0]):
                ramptype = 'EXIT'
            if coorconv == str(rdict[row[0]][1]):
                ramptype = 'ENTRANCE'        

        rtid = row[0]
        print "working route ID: " + str(rtid)
        measure = round(row[1],3)
        intnm = row[2]
        atgr = row[3]
        xcoor = row[4][0]
        ycoor = row[4][1]
        xycoor = row[4]
        intid = row[8]
        
        if intnm != None:
            st1 = roadwayname
            st2 = crossingroadwayname
            #we care about all streets, ramps or service roads
            if rtid in d:
                if measure in d[rtid]:
                    d[rtid][measure][0] = str(d[rtid][measure][0]) + ' & ' + str(st2)
                else:
                    d[rtid][measure] = ['%s & %s' % (st1, st2), atgr, xcoor, ycoor, ramptype,intid]
            else:
                d[rtid] = {}
                d[rtid][measure] = ['%s & %s' % (st1, st2), atgr, xcoor, ycoor, ramptype,intid]

        if row[4] not in rtiddict:
            rtiddict[xycoor] = str(rtid)
        else:
            if rtid not in rtiddict[xycoor]:
                rtiddict[xycoor] += '_%s' % (rtid)

    result = {}
    for key,value in rtiddict.items():
        if value not in result.values():
            result[key] = value
        elif value in result.values():
            if value + '*1' in result.values():
                if value + '*2' in result.values():
                    if value + '*3' in result.values():
                        print '...'
                    else:
                       result[key] = value + '*3' 
                else:
                    result[key] = value + '*2'
            else:
                result[key] = value + '*1'
            
    
del cursor
                 
#EDITS DICTIONARY TO REMOVE OVERLAYING INTERSECTIONS

#Make a X_Y key for every intersection and see if we get dupes
xydict = dict()
for routeids,events in d.items():
    for event, intersection in events.items():
        newkey = str(math.floor(intersection[2])) + "_" + str(math.floor(intersection[3]))
        intersection.append(routeids)
        intersection.append(event)
        if newkey not in xydict:
            xydict[newkey] = [intersection]
        else:
            xydict[newkey].append(intersection)

#UPDATING ATGRADE INFO
print "Updating table..."
with arcpy.da.InsertCursor(newfc, ['OLDINTERSECTIONID','NEWINTERSECTIONID','ROUTEID','MEASURE']) as icursor:
           
    count = 0
    for concoor,intlist in xydict.items():
        for int in intlist:
            count = count + 1
            print count
            icursor.insertRow((int[5], result[int[2],int[3]], int[6], int[7]))

del icursor

#SORTING INTERSECTION IDs               
with arcpy.da.UpdateCursor(newfc, ['NEWINTERSECTIONID']) as ucursor:

    for row in ucursor:
        intid = row[0]
        print intid

        intparts = intid.split('_')
        intparts.sort()

        intparts = '_'.join(intparts)
        
        row[0] = intparts

        ucursor.updateRow(row)

del ucursor


print 'done'