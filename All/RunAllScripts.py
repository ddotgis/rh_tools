
import arcpy, os, sys
import requests
import numpy
import operator
import math
import datetime
import smtplib, ssl
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email.encoders import encode_base64

scriptloc = sys.path[0]
os.chdir(scriptloc)
gdb = "scratch.gdb"
scratchgdb = os.path.join(scriptloc, gdb)


#whichstep = raw_input('Which step would you like to start from?: ')
#whichstep = int(whichstep)
whichstep = 1
currentstep = 1
while currentstep <= 18:

    if currentstep < whichstep:
        continue
    if currentstep == 1:
        print "Running Step 1"
        import BlockIntersections_Step1
    if currentstep == 2:
        print "Running Step 2"
        import SubBlockIntersections_Step2
    if currentstep == 3:    
        print "Running Step 3"    
        import IntersectionReferenceTable_Step3
    if currentstep == 4:    
        print "Running Step 4"
        import DetectRoadwayGaps_Step4
    if currentstep == 5:    
        print "Running Step 5"
        import GenerateBlocksToTable_Step5
    if currentstep == 6:    
        print "Running Step 6"
        import CreateBlocksWithNoInts_Step6
    if currentstep == 7:    
        print "Running Step 7"
        import PopulateBlockNullIDs_Step7
    if currentstep == 8:    
        print "Running Step 8"
        import BlockGapCorrection_Step8
    if currentstep == 9:    
        print "Running Step 9"
        import GenerateSubBlocksToTable_Step9
    if currentstep == 10:    
        print "Running Step 10"
        import PopulateSubBlockNullIDs_Step10
    if currentstep == 11:    
        print "Running Step 11"
        import SubBlockGapCorrection_Step11
    if currentstep == 12:    
        print "Running Step 12"
        import UpdateSubBlockRdSegID_Step12
    if currentstep == 13:    
        print "Running Step 13"
        import DeadEndShift_Step13
    if currentstep == 14:    
        print "Running Step 14"
        import AddDirectionality_Step14
    if currentstep == 15:    
        print "Running Step 15"
        import AddKeys_Step15
    if currentstep == 16:    
        print "Running Step 16"
        import GenerateIntersectionLegs_Step16
    if currentstep == 17:    
        print "Running Step 17"
        import PopulateAngleDirection_Step17
    if currentstep == 18:    
        print "Running Step 18"
        import PopulateApproachDirection_Step18
    currentstep += 1    


#SEND EMAIL AFTER SUCCESSFUL RUN

login = 'ddotgisautomation@gmail.com'
password = 'ddotgis123'
sender = 'ddotgisautomation@gmail.com'
receivers = ['osheen.safarian@dc.gov','faisal.khan@dc.gov','James.Graham2@dc.gov','davidy.jackson@dc.gov']


msg = MIMEMultipart()
msg['From'] = sender
msg['To'] = ", ".join(receivers)
msg['Subject'] = "LRS Scripts"

# Simple text message or HTML
TEXT = "Hello everyone,\n"
TEXT = TEXT + "\n"
TEXT = TEXT + "The LRS scripts have successfully completed running.\n"
TEXT = TEXT + "\n"
TEXT = TEXT + "Thanks,\n"
TEXT = TEXT + "DDOTGIS Automation"

msg.attach(MIMEText(TEXT))


smtpObj = smtplib.SMTP('smtp.gmail.com:587')
smtpObj.ehlo()
smtpObj.starttls()
smtpObj.login(login, password)
smtpObj.sendmail(sender, receivers, msg.as_string())

print "FINISHED"