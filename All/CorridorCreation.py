import arcpy, os, sys
import requests
import numpy
import operator
import math


scriptloc = sys.path[0]
os.chdir(scriptloc)
gdb = "scratch.gdb"
scratchgdb = os.path.join(scriptloc, gdb)


connection = r"Database Connections\DDOTLRS as LRSVIEWER.sde"
lrs_connection = r"Database Connections\DDOTLRS as LRSVIEWER.sde\Ddotlrs.RH.ReferenceFeatures"

rdwy = os.path.join(connection, 'Ddotlrs.RH.LRSN_Roadway')
blcks = os.path.join(lrs_connection, 'Ddotlrs.RH.Block')
sbint = os.path.join(lrs_connection, 'Ddotlrs.RH.SubBlockIntersection')
where = "(FROMDATE is null or FROMDATE<=CURRENT_TIMESTAMP) and (TODATE is null or TODATE>CURRENT_TIMESTAMP) and ROADTYPE = 1"

#stsegments = r'\\DDOTFILE01\UsersFiles\osafarian\rh_tools\RH-StreetSegments\scratch.gdb\RH_StreetSegments'

sbdict = {}
with arcpy.da.SearchCursor(sbint, ['IntersectionName','Shape@XY']) as scursor:
    for row in scursor:
        sbdict[row[1]] = row[0]

with arcpy.da.SearchCursor(rdwy, ['ROUTEID','ROUTENAME'], where_clause="(FROMDATE is null or FROMDATE<=CURRENT_TIMESTAMP) and (TODATE is null or TODATE>CURRENT_TIMESTAMP)") as scursor1:
    intdict = {}
    for row in scursor1:
        intdict[row[0]] = row[1]

del scursor1

lrsFdict = {}
lrsTdict = {}
with arcpy.da.SearchCursor(blcks,['RouteID','FromMeasure','ToMeasure','FromStreet','ToStreet']) as scursor:
    for row in scursor:
        rtid = row[0]
        fmeas = row[1]
        tmeas = row[2]
        fstr = row[3]
        tstr = row[4]

        if rtid not in lrsFdict:
            lrsFdict[rtid] = {}
            lrsFdict[rtid][fmeas] = {}
            lrsFdict[rtid][fmeas]['ToMeasure'] = tmeas
            lrsFdict[rtid][fmeas]['FromStreet'] = fstr
            lrsFdict[rtid][fmeas]['ToStreet'] = tstr
        else:
            lrsFdict[rtid][fmeas] = {}
            lrsFdict[rtid][fmeas]['ToMeasure'] = tmeas
            lrsFdict[rtid][fmeas]['FromStreet'] = fstr
            lrsFdict[rtid][fmeas]['ToStreet'] = tstr

        if rtid not in lrsTdict:
            lrsTdict[rtid] = {}
            lrsTdict[rtid][tmeas] = {}
            lrsTdict[rtid][tmeas]['FromMeasure'] = fmeas
            lrsTdict[rtid][tmeas]['FromStreet'] = fstr
            lrsTdict[rtid][tmeas]['ToStreet'] = tstr
        else:
            lrsTdict[rtid][tmeas] = {}
            lrsTdict[rtid][tmeas]['FromMeasure'] = fmeas
            lrsTdict[rtid][tmeas]['FromStreet'] = fstr
            lrsTdict[rtid][tmeas]['ToStreet'] = tstr


tablename = 'RH_CorridorTable'
tableloc = os.path.join(scratchgdb, tablename)
if not arcpy.Exists(tableloc):
    arcpy.CreateTable_management(scratchgdb, tablename)
    arcpy.AddField_management(tableloc, "RouteID", "TEXT", 50)
    arcpy.AddField_management(tableloc, "RouteName", "TEXT", 50)
    arcpy.AddField_management(tableloc, "RoadType", "SHORT", 50,field_domain='RoadType')
    arcpy.AddField_management(tableloc, "CorridorID", "TEXT", 50)
    arcpy.AddField_management(tableloc, "FromMeasure", "FLOAT", 50)
    arcpy.AddField_management(tableloc, "ToMeasure", "FLOAT", 50)
    arcpy.AddField_management(tableloc, "FromStreet", "TEXT", 50)
    arcpy.AddField_management(tableloc, "ToStreet", "TEXT", 50)
    arcpy.AddField_management(tableloc, "StartX", "DOUBLE", 50)
    arcpy.AddField_management(tableloc, "StartY", "DOUBLE", 50)
    arcpy.AddField_management(tableloc, "EndX", "DOUBLE", 50)
    arcpy.AddField_management(tableloc, "EndY", "DOUBLE", 50)
else:
    arcpy.TruncateTable_management(tableloc)

count = 0
with arcpy.da.InsertCursor(tableloc, ['RouteID','RouteName','RoadType','CorridorID','FromMeasure','ToMeasure','FromStreet','ToStreet']) as icursor:
    with arcpy.da.SearchCursor(rdwy, ['ROUTEID','SHAPE@','ROUTENAME','ROADTYPE'],where_clause=where) as scursor:
        for row in scursor:
            rtid = row[0]
            rtname = row[2]
            rdtype = row[3]


            if rtid == '12059472':
                print "OK"

            partnum = 0
            segno = 0
            for part in row[1]:
                segno += 1
                print("Part {}:".format(partnum))

                startxcoor = part[0].X
                startycoor = part[0].Y
                endxcoor = part[-1].X
                endycoor = part[-1].Y

                try:
                    newfst = sbdict[startxcoor,startycoor]
                    newtst = sbdict[endxcoor,endycoor]
                    newfst = newfst.split(' & ')
                    newtst = newtst.split(' & ')
    
                    for name in newfst:
                        if name == rtname:
                            newfst.remove(name)
                    fnamestr = ''
                    for name in newfst:
                        if fnamestr == '':
                            fnamestr += str(name)
                        else:
                            fnamestr += '/%s' % name
                                                   
                    for name in newtst:
                        if name == rtname:
                            newtst.remove(name)
                    tnamestr = ''
                    for name in newtst:
                        if tnamestr == '':
                            tnamestr += str(name)
                        else:
                            tnamestr += '/%s' % name
                except KeyError:
                    for fMeas in lrsFdict[rtid]:
                        if fMeas >= beginmeas - 1 and fMeas <= beginmeas + 1:
                            fnamestr = lrsFdict[rtid][fMeas]['FromStreet']
                    for tMeas in lrsTdict[rtid]:
                        if tMeas >= endmeas - 1 and tMeas <= endmeas + 1:
                            tnamestr = lrsTdict[rtid][tMeas]['ToStreet']

                corridorID = rtid + '_' + str(segno)
            
                pntnum = 0
                for pnt in part:
                    if pntnum == 0:
                        beginmeas = pnt.M
                    endmeas = pnt.M
                    pntnum += 1
#                for fMeas in lrsFdict[rtid]:
#                    if fMeas >= beginmeas - 1 and fMeas <= beginmeas + 1:
#                        newfst = lrsFdict[rtid][fMeas]['FromStreet']
#                for tMeas in lrsTdict[rtid]:
#                    if tMeas >= endmeas - 1 and tMeas <= endmeas + 1:
#                        newtst = lrsTdict[rtid][tMeas]['ToStreet']
                icursor.insertRow((rtid, rtname, rdtype, corridorID, beginmeas, endmeas, fnamestr, tnamestr))
                

corridorlayer = os.path.join(scratchgdb, 'RH_CorridorLayer')

rdwyq = "rdwyq"
where = "(FROMDATE is null or FROMDATE<=CURRENT_TIMESTAMP) and (TODATE is null or TODATE>CURRENT_TIMESTAMP)"
if arcpy.Exists(rdwyq):
    arcpy.Delete_management(rdwyq)
    arcpy.MakeFeatureLayer_management(rdwy, rdwyq, where)
else:
    arcpy.MakeFeatureLayer_management(rdwy, rdwyq, where)
    
if not arcpy.Exists(corridorlayer):
    arcpy.MakeRouteEventLayer_lr(rdwyq, 'ROUTEID', tableloc, 'RouteID LINE FromMeasure ToMeasure', corridorlayer)
else:
    arcpy.Delete_management(corridorlayer)
    arcpy.MakeRouteEventLayer_lr(rdwyq, 'ROUTEID', tableloc, 'RouteID LINE FromMeasure ToMeasure', corridorlayer)

corridorfc = os.path.join(scratchgdb, 'RH_Corridor')
if not arcpy.Exists(corridorfc):
    arcpy.CopyFeatures_management(corridorlayer, corridorfc)
else:
    arcpy.Delete_management(corridorfc)
    arcpy.CopyFeatures_management(corridorlayer, corridorfc)  


with arcpy.da.UpdateCursor(corridorfc, ['StartX','StartY','EndX','EndY','SHAPE@','CorridorID']) as ucursor:
    for row in ucursor:
        print row[5]
        if row[4] == None:
            continue
        row[0] = row[4].firstPoint.X
        row[1] = row[4].lastPoint.X
        row[2] = row[4].firstPoint.Y
        row[3] = row[4].lastPoint.Y
        ucursor.updateRow(row)

print "DONE"














