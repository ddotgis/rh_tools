#----------------------------------------------------------------------
#This script populates the Angle, Directionality and ApproachID fields.
#----------------------------------------------------------------------

import arcpy, os, sys
from math import *
import requests
import numpy
import operator

def calculate_initial_compass_bearing(pointA, pointB):
    """
    Calculates the bearing between two points.
    :Parameters:
      - `pointA: The tuple representing the latitude/longitude for the
        first point. Latitude and longitude must be in decimal degrees
      - `pointB: The tuple representing the latitude/longitude for the
        second point. Latitude and longitude must be in decimal degrees
 
    :Returns:
      The bearing in degrees
 
    :Returns Type:
      float
    """
    if (type(pointA) != tuple) or (type(pointB) != tuple):
        raise TypeError("Only tuples are supported as arguments")
 
    lat1 = radians(pointA[0])
    lat2 = radians(pointB[0])
 
    diffLong = radians(pointB[1] - pointA[1])
 
    x = sin(diffLong) * cos(lat2)
    y = cos(lat1) * sin(lat2) - (sin(lat1)
            * cos(lat2) * cos(diffLong))
 
    initial_bearing = atan2(x, y)
 
    # Now we have the initial bearing but math.atan2 return values
    # from -180 to + 180 which is not what we want for a compass bearing
    # The solution is to normalize the initial bearing as shown below
    initial_bearing = degrees(initial_bearing)
    compass_bearing = (initial_bearing + 360) % 360
 
    return compass_bearing

def getTextualBearing(thisAz):

    if (thisAz >= 0 and thisAz <=30):
        return "North"

    if (thisAz >=30 and thisAz <=70):
        return "Northeast"

    if (thisAz >=70 and thisAz <=110):
        return "East"

    if (thisAz >= 110 and thisAz <=150):
        return "Southeast"

    if (thisAz >= 150 and thisAz <=210):
        return "South"
    
    if (thisAz >= 210 and thisAz <=250):
        return "Southwest"

    if (thisAz >= 250 and thisAz <=290):
        return "West"

    if (thisAz >= 290 and thisAz <=330):
        return "Northwest"

    if (thisAz >= 330 and thisAz <=360):
        return "North"

def calcBearing(begin_x, begin_y, end_x, end_y):
    #see http://stackoverflow.com/questions/5058617/bearing-between-two-points
    angle = degrees(atan2(end_y - begin_y, end_x - begin_x))
    bearing = (90 - angle) % 360
    return bearing

def getFields(inputFileName):
    '''This function returns a list of the fields in a spatial
    or tabular dataset'''

    desc = arcpy.Describe(inputFileName)
    fields = []
    for field in desc.fields:
        fields.append(field.name)

    return fields

def getXYsFromRouteIDandMs(routeid,locations=[]):
    '''
    Retrieve XY locations from Roads and Highways LRS route id data Roads and Highways LRS rest service
    along a single route.
    '''

    locationlist = []
    
    try:
        if locations:
            for measure in locations:
                
                locationlist.append({"routeId":routeid,"measure":measure})
    except:
        raise Exception,  "Error. No location data"

    payload = {"locations": str(locationlist),
               "f":'json'}

    r1 = requests.get("https://rh.dcgis.dc.gov/dcgis/rest/services/DDOT/RoadwayBlocks/MapServer/exts/LRSServer/networkLayers/4/measureToGeometry", params=payload)
    try:
        results = r1.json()
    except:
        raise Exception,  "Error.  Cannot parse json.  Possibly invalid StreetSegID"


    return results
    
scriptloc = sys.path[0]
os.chdir(scriptloc)
gdb = "scratch.gdb"
scratchgdb = os.path.join(scriptloc, gdb)

connectiondev = r"Database Connections\DDOTLRS as LRSVIEWER.sde"
connection = r"Database Connections\DDOTLRS as CARSSVIEWER.sde"

routeintfc = os.path.join(connectiondev, 'RH.LRSI_RouteIntersection')
intapproaches = os.path.join(scratchgdb, 'RH_Approaches')
rhrdwyseg = os.path.join(scratchgdb, 'RH_SubBlock')

with arcpy.da.SearchCursor(rhrdwyseg, ['RouteID','FromMeasure','ToMeasure','FromIntersectionID','ToIntersectionID']) as scursor:
    d = {}
    for row in scursor:
        if row[0] in d:
            d[row[0]][row[3]] = row[1]
            d[row[0]][row[4]] = row[2]
        else:
            d[row[0]] = {}
            d[row[0]][row[3]] = row[1]
            d[row[0]][row[4]] = row[2]

with arcpy.da.UpdateCursor(intapproaches, ['RouteID','IntersectionID','FromMeasure','ToMeasure','Angle','Directionality','ApproachID','SubBlockID']) as ucursor:
    errorcount = 0
    for row in ucursor:
        rtid = str(row[0])
        intid = row[1]
        fmeas = row[2]
        tmeas = row[3]
        angle = row[4]
        appid = row[6]
        rdwyseg = row[7]

        print rtid, intid

        try:
            intmeas = float(d[rtid][intid])
        except KeyError:
            errorcount += 1
            continue
            
        if intmeas <= (fmeas + .01) and intmeas >= (fmeas - .01):
            appangmeas = tmeas
        elif intmeas <= (tmeas + .01) and intmeas >= (tmeas - .01):
            appangmeas = fmeas

        cdict = getXYsFromRouteIDandMs(rtid, locations=[intmeas, appangmeas])
        if cdict['locations'][0]['geometry'] is None:
            cdict = getXYsFromRouteIDandMs(rtid, locations=[(intmeas - 0.001), appangmeas])
            if cdict['locations'][0]['geometry'] is None:
                continue
        elif cdict['locations'][1]['geometry'] is None:
            #cdict = getXYsFromRouteIDandMs(rtid, locations=[intmeas, appangmeas])
            continue
        xcoord1 = cdict['locations'][0]['geometry']['x']
        ycoord1 = cdict['locations'][0]['geometry']['y']
        xcoord2 = cdict['locations'][1]['geometry']['x']
        ycoord2 = cdict['locations'][1]['geometry']['y']

        orig_x = xcoord2
        orig_y = ycoord2

        dest_intersection_x = xcoord1
        dest_intersection_y = ycoord1
        
        bearing = calcBearing(dest_intersection_x, dest_intersection_y, orig_x, orig_y)
        direction_from_nearest_intersection = getTextualBearing(bearing)

        row[4] = bearing
        row[5] = direction_from_nearest_intersection
        row[6] = round(row[4])
 
        ucursor.updateRow((row))
        print direction_from_nearest_intersection 

print "FINISHED"
print str(errorcount) + "ERRORS"
