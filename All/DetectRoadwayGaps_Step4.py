import arcpy, os, sys
import requests
import numpy
import operator
import math


scriptloc = sys.path[0]
os.chdir(scriptloc)
gdb = "scratch.gdb"
scratchgdb = os.path.join(scriptloc, gdb)

os.chdir("..")
intgdb = os.path.join(os.getcwd(), 'RH-Intersections\scratch.gdb')
connection = r"Database Connections\DDOTLRS as LRSVIEWER.sde"
connectiondev = r"Database Connections\DDOTGISDEV as RH user.sde"

rdwy = os.path.join(connection, 'Ddotlrs.RH.LRSN_Roadway')
stsegments = os.path.join(connection, 'Ddotlrs.RH.LRSE_StreetSegment')
where = "(FROMDATE is null or FROMDATE<=CURRENT_TIMESTAMP) and (TODATE is null or TODATE>CURRENT_TIMESTAMP)"

tablename = 'RH_RoadwayGaps'
tableloc = os.path.join(scratchgdb, tablename)
if not arcpy.Exists(tableloc):
    arcpy.CreateTable_management(scratchgdb, tablename)
    arcpy.AddField_management(tableloc, "RouteID", "TEXT", 50)
    arcpy.AddField_management(tableloc, "RouteName", "TEXT", 50)
    arcpy.AddField_management(tableloc, "Measure", "DOUBLE", 50)
    arcpy.AddField_management(tableloc, "X_Coord", "DOUBLE", 50)
    arcpy.AddField_management(tableloc, "Y_Coord", "DOUBLE", 50)
else:
    arcpy.TruncateTable_management(tableloc)

count = 0
with arcpy.da.InsertCursor(tableloc, ['RouteID','RouteName','Measure','X_Coord','Y_Coord']) as icursor:
    with arcpy.da.SearchCursor(rdwy, ['ROUTEID','SHAPE@','ROUTENAME'],where_clause=where) as scursor:
        for row in scursor:
            rtid = row[0]
            rtname = row[2]

            if rtid == '12059472':
                print "OK"
            partnum = 0
            if len(row[1]) == 1:
                continue
            for part in row[1]:
                print("Part {}:".format(partnum))
                pntnum = 0
                for pnt in part:
                    thisPnt = pnt
                    if partnum > 0 and pntnum == 0:
                        icursor.insertRow((rtid,rtname,thisPnt.M,thisPnt.X,thisPnt.Y))
                        icursor.insertRow((rtid,rtname,lastPnt.M,lastPnt.X,lastPnt.Y))
                    if pnt:
                        print("{}, {}, {}".format(pnt.X, pnt.Y,pnt.M))
                        pntnum += 1
                    else:
                        print("Interior Ring:")
                        pntnum += 1
                    lastPnt = thisPnt
                #icursor.insertRow((row[0],row[2], pnt.M))


                partnum += 1
