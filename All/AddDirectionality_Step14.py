#--------------------------------------------------------------------------------
#This script populates the Directionality field in both SubBlocks and Blocks.
#--------------------------------------------------------------------------------
import arcpy, os
import os, sys
from pprint import pprint
import requests
import numpy
import operator
import math

scriptloc = sys.path[0]
os.chdir(scriptloc)
gdb = "scratch.gdb"
scratchgdb = os.path.join(scriptloc, gdb)
connection = r"Database Connections\DDOTLRS as LRSVIEWER.sde" 

rdwyseg = os.path.join(connection, 'Ddotlrs.RH.LRSE_RoadwaySegment')
subblock = os.path.join(scratchgdb, 'RH_SubBlock')
block = os.path.join(scratchgdb, 'RH_Block')
subblocktbl = os.path.join(scratchgdb, 'RH_SubBlockTable')
blocktbl = os.path.join(scratchgdb, 'RH_BlockTable')

print "Creating roadway and directionality dictionary..."
rdsegdic = {}
with arcpy.da.SearchCursor(rdwyseg, ['ROADWAYSEGID','DIRECTIONALITY'],where_clause="(FROMDATE is null or FROMDATE<=CURRENT_TIMESTAMP) and (TODATE is null or TODATE>CURRENT_TIMESTAMP)") as scursor:
    for row in scursor:
        rdsegid = row[0]
        direct = row[1]

        rdsegdic[rdsegid] = direct

print "Updating SubBlocks..."
blockdirdict = {}
with arcpy.da.UpdateCursor(subblocktbl, ['RoadwaySegID','Directionality','BlockID']) as ucursor:
    for row in ucursor:
        if row[0] == None:
            continue
        if int(row[0]) in rdsegdic:
            newdirect = rdsegdic[int(row[0])]
            blockdirdict[row[2]] = newdirect
            row[1] = newdirect
            ucursor.updateRow((row))
        else:
            continue
del ucursor

print "Updating Blocks..."
with arcpy.da.UpdateCursor(blocktbl, ['BlockID', 'Directionality']) as ucursor:
    for row in ucursor:
        if row[0] in blockdirdict:
            row[1] = blockdirdict[row[0]]
            ucursor.updateRow((row))
        else:
            continue
del ucursor

blockdirdict = {}
with arcpy.da.UpdateCursor(subblock, ['RoadwaySegID','Directionality','BlockID']) as ucursor:
    for row in ucursor:
        if row[0] == None:
            continue
        if int(row[0]) in rdsegdic:
            newdirect = rdsegdic[int(row[0])]
            blockdirdict[row[2]] = newdirect
            row[1] = newdirect
            ucursor.updateRow((row))
        else:
            continue
del ucursor

print "Updating Blocks..."
with arcpy.da.UpdateCursor(block, ['BlockID', 'Directionality']) as ucursor:
    for row in ucursor:
        if row[0] in blockdirdict:
            row[1] = blockdirdict[row[0]]
            ucursor.updateRow((row))
        else:
            continue
del ucursor











