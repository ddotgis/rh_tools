#-------------------------------------------------
#This script dynamically creates a SubBlock table.
#-------------------------------------------------

import arcpy, os, sys
import requests
import numpy
import operator
import math


scriptloc = sys.path[0]
os.chdir(scriptloc)
gdb = "scratch.gdb"
scratchgdb = os.path.join(scriptloc, gdb)

connection = r"Database Connections\DDOTLRS as LRSVIEWER.sde"
connectiondev = r"Database Connections\DDOTGISDEV as RH user.sde"

roadgap = os.path.join(scratchgdb, 'RH_RoadwayGaps')
routeintfc = os.path.join(connection, 'RH.LRSI_RouteIntersection')
rhrdwyint = os.path.join(scratchgdb, 'RH_SubBlockIntersections')
rdwy = os.path.join(connection, 'Ddotlrs.RH.LRSN_Roadway')
enhancedsegtbl = os.path.join(scratchgdb, 'RH_SubBlockTable')
sbblkerror = os.path.join(scratchgdb, 'SubBlockErrorTable')
stsegments = os.path.join(scratchgdb, 'RH_BlockTable')

#Change this measure, which is in meters, to indicate the tolerance of the minimum length of last segment in route. 
routeEndClip = 2

tablename = 'RH_SubBlockTable'
if not arcpy.Exists(enhancedsegtbl):
    arcpy.CreateTable_management(scratchgdb, tablename)
    arcpy.AddField_management(enhancedsegtbl, "RouteID", "TEXT", 50)
    arcpy.AddField_management(enhancedsegtbl, "RouteName", "TEXT", 50)
    arcpy.AddField_management(enhancedsegtbl, "SubBlockID", "TEXT", 50)
    arcpy.AddField_management(enhancedsegtbl, "FromMeasure", "FLOAT", 50)
    arcpy.AddField_management(enhancedsegtbl, "ToMeasure", "FLOAT", 50)
    arcpy.AddField_management(enhancedsegtbl, "FromIntersectionID", "TEXT", 80)
    arcpy.AddField_management(enhancedsegtbl, "ToIntersectionID", "TEXT", 80)
    arcpy.AddField_management(enhancedsegtbl, "RoadType", "SHORT", 80,field_domain='RoadType')
    arcpy.AddField_management(enhancedsegtbl, "BlockID", "TEXT", 80)
    arcpy.AddField_management(enhancedsegtbl, "RoadwaySegID", "TEXT", 100)
    arcpy.AddField_management(enhancedsegtbl, "Directionality", "SHORT", 20,field_domain='Directionality')
    arcpy.AddField_management(enhancedsegtbl, "SubBlockKey", "TEXT", 100)
    arcpy.AddField_management(enhancedsegtbl, "BlockKey", "TEXT", 100)
    arcpy.AddField_management(enhancedsegtbl, "FromIntersectionKey","TEXT", 100)
    arcpy.AddField_management(enhancedsegtbl, "ToIntersectionKey","TEXT", 100)
    arcpy.AddField_management(enhancedsegtbl, "FromSubBlock","TEXT", 100)
    arcpy.AddField_management(enhancedsegtbl, "ToSubBlock","TEXT", 100)
else:
    arcpy.TruncateTable_management(enhancedsegtbl)

with arcpy.da.SearchCursor(roadgap, ['RouteID','Measure']) as scursor:
    roadgapdic = {}
    for row in scursor:
        if row[0] in roadgapdic:
            roadgapdic[row[0]].append(row[1])
            roadgapdic[row[0]].sort()
        else:
            roadgapdic[row[0]] = []
            roadgapdic[row[0]].append(row[1])
del scursor

streettable = {}
with arcpy.da.SearchCursor(stsegments, ['RouteID','FromMeasure','ToMeasure','BlockID']) as scursor:
    for row in scursor:
        routeid = row[0]
        fmeas = row[1]
        tmeas = row[2]
        stsegid = row[3]
        if routeid in streettable:
            streettable[routeid][stsegid] = {}
            streettable[routeid][stsegid]['FROMMEASURE'] = fmeas
            streettable[routeid][stsegid]['TOMEASURE'] = tmeas
        else:
            streettable[routeid] = {}
            streettable[routeid][stsegid] = {}
            streettable[routeid][stsegid]['FROMMEASURE'] = fmeas
            streettable[routeid][stsegid]['TOMEASURE'] = tmeas

with arcpy.da.SearchCursor(rhrdwyint, ['INTERSECTIONID','GRADE']) as scursor1:
    intdict = {}
    for row in scursor1:
        if row[1] == 1:
            continue
        intid = str(row[0])
        intidlist = intid.split('_')
        for s in intidlist:
            if row[0] in intdict:
                intdict[row[0]].append(s)
            else:
                intdict[row[0]] = [s]
del scursor1

try:
    with arcpy.da.SearchCursor(rdwy, ['ROUTEID','SHAPE.STLength()'], where_clause="(FROMDATE is null or FROMDATE<=CURRENT_TIMESTAMP) and (TODATE is null or TODATE>CURRENT_TIMESTAMP)") as scursor2:
        rdwydic = {}
        for row in scursor2:
            rdwydic[row[0]] = row[1]

    del scursor2
except RuntimeError:
    print "LRS Database connection not connecting"
    sys.exit()
    #somehow log this

try:
    with arcpy.da.SearchCursor(routeintfc, ['ROUTEID','Measure','FROMDATE','TODATE','AtGrade'],where_clause="(FROMDATE is null or FROMDATE<=CURRENT_TIMESTAMP) and (TODATE is null or TODATE>CURRENT_TIMESTAMP) and (AtGrade is null or AtGrade <> 1)", sql_clause=(None, 'ORDER BY ROUTEID, Measure')) as scursor3:
            routeintdic = {}
            for row in scursor3:
                if row[0] in routeintdic:
                    routeintdic[row[0]].append(row[1])
                else:
                    routeintdic[row[0]] = [row[1]]

    del scursor3
except RuntimeError:
    print "LRS Database connection not connecting"
    sys.exit()
    #somehow log this

try:
    with arcpy.da.SearchCursor(rdwy, ['ROUTEID','ROUTENAME'], where_clause="(FROMDATE is null or FROMDATE<=CURRENT_TIMESTAMP) and (TODATE is null or TODATE>CURRENT_TIMESTAMP)") as scursor4:
        rdnamedic = {}
        for row in scursor4:
            rdnamedic[row[0]] = row[1]

    del scursor4
except RuntimeError:
    print "LRS Database connection not connecting"
    sys.exit()
    #somehow log this

try:
    with arcpy.da.SearchCursor(rdwy, ['RouteID','RoadType'], where_clause="(FROMDATE is null or FROMDATE<=CURRENT_TIMESTAMP) and (TODATE is null or TODATE>CURRENT_TIMESTAMP)") as scursor5:
        rdtypedict = {}
        for row in scursor5:
            rdtypedict[row[0]] = row[1]

    del scursor5
except RuntimeError:
    print "LRS Database connection not connecting"
    sys.exit()
    #somehow log this

#HOW FAR APART INTERSECTIONS SHOULD BE MINIMUM(METERS)
intsep = .1

prevrtid = 0
#prevrtid = 1

#UPDATING TABLE
with arcpy.da.InsertCursor(enhancedsegtbl, ['RouteID','FromMeasure','ToMeasure','FromIntersectionID', 'ToIntersectionID','RouteName','BlockID','RoadType']) as icursor:
    with arcpy.da.SearchCursor(routeintfc, ['INTERSECTIONNAME','ROUTEID','FEATUREID','Measure','AtGrade', 'SHAPE@X', 'SHAPE@Y'],
                               where_clause="(FROMDATE is null or FROMDATE<=CURRENT_TIMESTAMP) and (TODATE is null or TODATE>CURRENT_TIMESTAMP) and (AtGrade is null or AtGrade <> 1)",
                               sql_clause=(None, 'ORDER BY ROUTEID, Measure')) as scursor:
        meas = None
        rtsegno = 1

        for row in scursor:
            print row[1], row[2], row[3]
            if row[1] == '47029882':
                continue
            if row[1] == '12064672' and row[3] > 4777:
                print "OK"

            currintname = row[0]
            currrtid = row[1]
            currftid = row[2]
            currmeas = row[3]
            currxcoor = row[5]
            currycoor = row[6]

            try:
                rdwydic[currrtid]
            except KeyError:
                continue
            roadtype = rdtypedict[currrtid]
            #If this is the first intersection we are looking at, there is no 'previous intersection', so this is our starting point.
            if prevrtid == 0:
                if currmeas <> 0:
                    rdname = rdnamedic[currrtid]
                    getkeyto = [k for k, v in intdict.items() if currrtid in k and currftid in k]
                    if len(getkeyto) > 1:
                        toid = 'null1'
                    elif len(getkeyto) == 0:
                        toid = 'null2'
                    else:
                        toid = getkeyto[0]
                    if rdtypedict[currrtid] == 1:
                        for item in streettable[currrtid]:
                            if 0.0 >= streettable[currrtid][item]['FROMMEASURE'] and currmeas <= (streettable[currrtid][item]['TOMEASURE'] + 1):
                                stid = item
                                break
                            else:
                                stid = 'ERROR'
                                continue
                    else:
                        stid = 'Not a Street'

                    icursor.insertRow((currrtid, 0, currmeas, 'Dead End', toid, rdname, stid,roadtype))
                    prevrtid = currrtid
                    prevmeas = currmeas
                    prevftid = currftid
                    prevxcoor = currxcoor
                    prevycoor = currycoor
                    continue
                else:
                    prevrtid = currrtid
                    prevmeas = currmeas
                    prevftid = currftid
                    prevxcoor = currxcoor
                    prevycoor = currycoor
                    continue

            #If current intersection is on a different route than the last one
            elif currrtid <> prevrtid:
                if prevmeas != rdwydic[prevrtid]:
                    getkeyfrom = [k for k, v in intdict.items() if prevrtid in k and prevftid in k]
                    if len(getkeyfrom) > 1:
                        fromid = 'null1'
                    elif len(getkeyfrom) == 0:
                        fromid = 'null2'
                    else:
                        fromid = getkeyfrom[0]
                    rdname = rdnamedic[prevrtid]
                    roadlen = rdwydic[prevrtid]
                    if rdtypedict[prevrtid] == 1:
                        for item in streettable[prevrtid]:
                            if 0.0 >= streettable[prevrtid][item]['FROMMEASURE'] and roadlen <= (streettable[prevrtid][item]['TOMEASURE'] + 1):
                                stid = item
                                break
                            else:
                                stid = 'ERROR'
                                continue
                    else:
                        stid = 'Not a Street'
                    roadtype = rdtypedict[prevrtid]
                    if abs(roadlen - prevmeas) >= routeEndClip:
                        icursor.insertRow((prevrtid, prevmeas, rdwydic[prevrtid], fromid, 'Dead End', rdname, stid, roadtype))

                #If measure is at 0.0. This means that the first intersection of the route starts at the beginning of the geometry.
                if currmeas == 0.0:

                    #If there is only one intersection on this route (indicates that it is a dead end)
                    if len(routeintdic[currrtid]) == 1:
                        rtsegno = 1
                        rdsegindex = str(currrtid) + '_' + str(rtsegno)
                        getkeyto = [k for k, v in intdict.items() if currrtid in k and currftid in k]
                        if len(getkeyto) > 1:
                            toid = 'null1'
                        elif len(getkeyto) == 0:
                            toid = 'null2'
                        else:
                            toid = getkeyto[0]
                        roadlen = rdwydic[currrtid]
                        #rdsegid = toid + '-' + 'Dead End'
                        rdname = rdnamedic[currrtid]
                        if rdtypedict[currrtid] == 1:
                            for item in streettable[currrtid]:
                                if 0.0 >= streettable[currrtid][item]['FROMMEASURE'] and roadlen <= (streettable[currrtid][item]['TOMEASURE'] + 1):
                                    stid = item
                                    break
                                else:
                                    stid = 'ERROR'
                                    continue
                        else:
                            stid = 'Not a Street'

                        icursor.insertRow((currrtid, 0.0, roadlen, toid, 'Dead End', rdname,stid,roadtype))

                    #If there are more than one intersections on this route.
                    else:

                        #If intersections are close to each other
                        intsonroute = len(routeintdic[currrtid])
                        m = 0
                        doesintcount = 'n'
                        while m <= (intsonroute - 1):
                            intval = routeintdic[currrtid][m]
                            if intval >= .001:
                                doesintcount = 'y'
                                break
                            m += 1

                        if doesintcount == 'n':
                            rtsegno = 1
                            rdsegindex = str(currrtid) + '_' + str(rtsegno)
                            getkeyto = [k for k, v in intdict.items() if currrtid in k and currftid in k]
                            if len(getkeyto) > 1:
                                toid = 'null1'
                            elif len(getkeyto) == 0:
                                toid = 'null2'
                            else:
                                toid = getkeyto[0]
                            roadlen = rdwydic[currrtid]
                            #rdsegid = toid + '-' + 'Dead End'
                            rdname = rdnamedic[currrtid]
                            if rdtypedict[currrtid] == 1:
                                for item in streettable[currrtid]:
                                    if 0.0 >= streettable[currrtid][item]['FROMMEASURE'] and roadlen <= (streettable[currrtid][item]['TOMEASURE'] + 1):
                                        stid = item
                                        break
                                    else:
                                        stid = 'ERROR'
                                        continue
                            else:
                                stid = 'Not a Street'

                            icursor.insertRow((currrtid, 0.0, roadlen, toid, 'Dead End', rdname,stid,roadtype))
                            rtsegno += 1

                        else:
                            prevrtid = currrtid
                            prevmeas = currmeas
                            prevftid = currftid
                            prevxcoor = currxcoor
                            prevycoor = currycoor

                #If measure is not 0.0. This means that the route starts with a dead end instead of an intersection.
                elif currmeas <> 0.0:
                    rtsegno = 1
                    rdsegindex = str(currrtid) + '_' + str(rtsegno)
                    getkeyto = [k for k, v in intdict.items() if currrtid in k and currftid in k]
                    if len(getkeyto) > 1:
                        toid = 'null1'
                    elif len(getkeyto) == 0:
                        toid = 'null2'
                    else:
                        toid = getkeyto[0]
                    #rdsegid = 'Dead End' + '-' + toid
                    rdname = rdnamedic[currrtid]
                    if rdtypedict[currrtid] == 1:
                        for item in streettable[currrtid]:
                            if 0.0 >= streettable[currrtid][item]['FROMMEASURE'] and currmeas <= (streettable[currrtid][item]['TOMEASURE'] + 1):
                                stid = item
                                break
                            else:
                                stid = 'ERROR'
                                continue
                    else:
                        stid = 'Not a Street'


                    icursor.insertRow((currrtid, 0.0, currmeas, 'Dead End', toid, rdname,stid,roadtype))

                rtsegno = 1
                prevrtid = currrtid
                prevmeas = currmeas
                prevftid = currftid
                prevxcoor = currxcoor
                prevycoor = currycoor
                continue

            #If the current intersection and the last intersection are on the same route (RouteID)
            elif currrtid == prevrtid:

                #If current intersection is not the last in its route.
                if currmeas < (max(routeintdic[currrtid]) - .001):

                    #If current and previous intersections are less than the specified distance (intsep) apart.
                    if currmeas < (prevmeas + intsep):
                        continue

                    #If current and previous intersections are equal to, or more than, the specified distance apart (intsep).
                    elif currmeas > (prevmeas + intsep):
                        frommeas = prevmeas
                        tomeas = currmeas
                        rdsegindex = str(currrtid) + '_' + str(rtsegno)

                        getkeyfrom = [k for k, v in intdict.items() if prevrtid in k and prevftid in k]
                        getkeyto = [k for k, v in intdict.items() if currrtid in k and currftid in k]


                        if len(getkeyfrom) > 1:
                            fromid = 'null1'
                        elif len(getkeyfrom) == 0:
                            fromid = 'null2'
                        else:
                            fromid = getkeyfrom[0]
                        if len(getkeyto) > 1:
                            toid = 'null1'
                        elif len(getkeyto) == 0:
                            toid = 'null2'
                        else:
                            toid = getkeyto[0]
                        #rdsegid = fromid + '-' + toid
                        rdname = rdnamedic[currrtid]
                        if rdtypedict[currrtid] == 1:
                            for item in streettable[currrtid]:
                                if frommeas >= (streettable[currrtid][item]['FROMMEASURE'] - 1) and tomeas <= (streettable[currrtid][item]['TOMEASURE'] + 1):
                                    stid = item
                                    break
                                else:
                                    stid = 'ERROR'
                                    continue
                        else:
                            stid = 'Not a Street'

                        icursor.insertRow((currrtid, frommeas, tomeas, fromid, toid, rdname,stid,roadtype))
                        rtsegno += 1

                #If current intersection is the last in its route.
                else:

                    #If the current intersection is the last one in its route and at the very end of the geometry.
                    if (currmeas <= (round(rdwydic[currrtid], 3) + .002)) and (currmeas >= (round(rdwydic[currrtid], 3) - .002)):

                        #If the current intersection is less than specified distance away from last one.
                        if currmeas < (prevmeas + intsep):
                            continue

                        #If current intersection is more than specified distance away from last one.
                        else:
                            rdsegindex = str(prevrtid) + '_' + str(rtsegno)
                            getkeyfrom = [k for k, v in intdict.items() if prevrtid in k and prevftid in k]
                            getkeyto = [k for k, v in intdict.items() if currrtid in k and currftid in k]
                            if len(getkeyfrom) > 1:
                                fromid = 'null1'
                            elif len(getkeyfrom) == 0:
                                fromid = 'null2'
                            else:
                                fromid = getkeyfrom[0]
                            if len(getkeyto) > 1:
                                toid = 'null1'
                            elif len(getkeyto) == 0:
                                toid = 'null2'
                            else:
                                toid = getkeyto[0]
                            #rdsegid = fromid + '-' + toid
                            rdname = rdnamedic[prevrtid]
                            if rdtypedict[prevrtid] == 1:
                                for item in streettable[prevrtid]:
                                    if prevmeas >= (streettable[prevrtid][item]['FROMMEASURE'] - 1) and round(rdwydic[prevrtid], 3) <= (streettable[prevrtid][item]['TOMEASURE'] + 1):
                                        stid = item
                                        break
                                    else:
                                        stid = 'ERROR'
                                        continue
                            else:
                                stid = 'Not a Street'

                            icursor.insertRow((prevrtid, prevmeas, round(rdwydic[prevrtid], 3), fromid, toid, rdname,stid,roadtype))

                    #If the current intersection is the last one in its route but not at the end of the geometry.
                    elif (currmeas <= (max(routeintdic[currrtid]) + .001)) and (currmeas >= (max(routeintdic[currrtid]) - .001)):

                        #If the current intersection is less than specified distance away from last one.
                        if currmeas < (prevmeas + intsep):
                            continue

                        #If current intersection is more than specified distance away from last one.
                        else:

                            #First, we create a segment from the previous intersection to the current one.
                            frommeas = prevmeas
                            tomeas = currmeas
                            rdsegindex = str(currrtid) + '_' + str(rtsegno)

                            getkeyfrom = [k for k, v in intdict.items() if prevrtid in k and prevftid in k]
                            getkeyto = [k for k, v in intdict.items() if currrtid in k and currftid in k]

                            if len(getkeyfrom) > 1:
                                fromid = 'null1'
                            elif len(getkeyfrom) == 0:
                                fromid = 'null2'
                            else:
                                fromid = getkeyfrom[0]
                            if len(getkeyto) > 1:
                                toid = 'null1'
                            elif len(getkeyto) == 0:
                                toid = 'null2'
                            else:
                                toid = getkeyto[0]
                            #rdsegid = fromid + '-' + toid
                            rdname = rdnamedic[currrtid]
                            if rdtypedict[currrtid] == 1:
                                for item in streettable[currrtid]:
                                    if frommeas >= (streettable[currrtid][item]['FROMMEASURE'] - 1) and tomeas <= (streettable[currrtid][item]['TOMEASURE'] + 1):
                                        stid = item
                                        break
                                    else:
                                        stid = 'ERROR'
                                        continue
                            else:
                                stid = 'Not a Street'

                            icursor.insertRow((currrtid, frommeas, tomeas, fromid, toid, rdname,stid,roadtype))

                            #Now, we create the final segment of this route, by using the measure of the current intersection, and the length of the roadway geometry.
                            rdsegindex = str(prevrtid) + '_' + str(rtsegno)
                            getkeyto = [k for k, v in intdict.items() if currrtid in k and currftid in k]
                            if len(getkeyto) > 1:
                                fromid = 'null1'
                            elif len(getkeyto) == 0:
                                fromid = 'null2'
                            else:
                                fromid = getkeyto[0]
                            #rdsegid = fromid + '-' + 'Dead End'
                            rdname = rdnamedic[currrtid]
                            if rdtypedict[currrtid] == 1:
                                for item in streettable[currrtid]:
                                    if currmeas >= (streettable[currrtid][item]['FROMMEASURE'] - 1) and round(rdwydic[currrtid], 3) <= (streettable[currrtid][item]['TOMEASURE'] + 1):
                                        stid = item
                                        break
                                    else:
                                        stid = 'ERROR'
                                        continue
                            else:
                                stid = 'Not a Street'

                            icursor.insertRow((currrtid, currmeas, round(rdwydic[currrtid], 3), fromid, 'Dead End', rdname,stid,roadtype))

            else:
                continue

            prevrtid = currrtid
            prevmeas = currmeas
            prevftid = currftid
            prevxcoor = currxcoor
            prevycoor = currycoor

del icursor
del scursor
