#--------------------------------------------------------------------------------
#This script updates 'Dead End' to a 5 digit coded ID. Example: 00001 would be the
#ID of the first Dead End that appears on a route, 00002 would be the second, etc.
#--------------------------------------------------------------------------------
import arcpy, os
import os, sys
from pprint import pprint
import requests
import numpy
import operator
import math

scriptloc = sys.path[0]
os.chdir(scriptloc)
gdb = "scratch.gdb"
scratchgdb = os.path.join(scriptloc, gdb)

connection = r"Database Connections\DDOTLRS as LRSVIEWER.sde"

subblocktbl = os.path.join(scratchgdb, 'RH_SubBlockTable')
blocktbl = os.path.join(scratchgdb, 'RH_BlockTable')
subblock = os.path.join(scratchgdb, 'RH_SubBlock')
block = os.path.join(scratchgdb, 'RH_Block')
rdwy = os.path.join(connection, 'Ddotlrs.RH.LRSN_Roadway')

with arcpy.da.SearchCursor(rdwy, ['RouteID','RoadType'], where_clause="(FROMDATE is null or FROMDATE<=CURRENT_TIMESTAMP) and (TODATE is null or TODATE>CURRENT_TIMESTAMP)") as scursor5:
    rdtypedict = {}
    for row in scursor5:
        rdtypedict[row[0]] = row[1]
del scursor5



iddigits = 5
deadendcount = 0
lastRtid = 0
blockdic = {}
with arcpy.da.UpdateCursor(blocktbl, ['RouteID','FromMeasure','ToMeasure','FromStreet','ToStreet','FromIntersectionID','ToIntersectionID','BlockID'], sql_clause=(None, 'ORDER BY RouteID, FromMeasure')) as ucursor:
    for row in ucursor:
        thisRtid = row[0]
        thisFmeas = row[1]
        thisTmeas = row[2]
        thisFstreet = row[3]
        thisTstreet = row[4]
        thisFint = row[5]
        thisTint = row[6]
        print row

        if thisRtid == '11003332' and thisFmeas > 511:
            print "OK"

        if thisRtid != lastRtid:
            deadendcount = 1

        if thisFint == 'Dead End':
            countlen = len(str(deadendcount))
            adddigits = iddigits - countlen
            count = 0
            newFstreet = ''
            while count != adddigits:
                count += 1
                newFstreet += '0'
            newFstreet += str(deadendcount)    
            deadendcount += 1
            row[3] = "DEAD END"
            row[5] = newFstreet
            blockdic[row[7]] = row[7].replace("Dead End", newFstreet, 1) 
            row[7] = row[7].replace("Dead End", newFstreet, 1)            

        if thisTint == 'Dead End':
            countlen = len(str(deadendcount))
            adddigits = iddigits - countlen
            count = 0
            newTstreet = ''
            while count != adddigits:
                count += 1
                newTstreet += '0'
            newTstreet += str(deadendcount)
            deadendcount +=1
            row[4] = "DEAD END"
            row[6] = newTstreet
            blockdic[row[7]] = row[7].replace("Dead End", newTstreet) 
            row[7] = row[7].replace("Dead End", newTstreet)
        ucursor.updateRow((row))   
        lastRtid = thisRtid
        lastFmeas = thisFmeas
        lastTmeas = thisTmeas
        lastFstreet = thisFstreet
        lastTstreet = thisTstreet
        lastFint = thisFint
        lastTint = thisTint

del ucursor

streettable = {}
with arcpy.da.SearchCursor(blocktbl, ['RouteID','FromMeasure','ToMeasure','BlockID']) as scursor:
    for row in scursor:
        routeid = row[0]
        fmeas = row[1]
        tmeas = row[2]
        stsegid = row[3]
        if routeid in streettable:
            streettable[routeid][stsegid] = {}
            streettable[routeid][stsegid]['FROMMEASURE'] = fmeas
            streettable[routeid][stsegid]['TOMEASURE'] = tmeas
        else:
            streettable[routeid] = {}
            streettable[routeid][stsegid] = {}
            streettable[routeid][stsegid]['FROMMEASURE'] = fmeas
            streettable[routeid][stsegid]['TOMEASURE'] = tmeas
del scursor

deadendcount = 0
lastRtid = 0
with arcpy.da.UpdateCursor(subblocktbl, ['RouteID','FromMeasure','ToMeasure','FromIntersectionID','ToIntersectionID','SubBlockID','BlockID'], sql_clause=(None, 'ORDER BY RouteID, FromMeasure')) as ucursor:
    for row in ucursor:
        thisRtid = row[0]
        thisFmeas = row[1]
        thisTmeas = row[2]
        thisFint = row[3]
        thisTint = row[4]
        print row

        if thisRtid == '12064672' and thisFmeas > 4777:
            print "OK"
        if thisRtid != lastRtid:
            deadendcount = 1

        if thisFint == 'Dead End':
            countlen = len(str(deadendcount))
            adddigits = iddigits - countlen
            count = 0
            newFstreet = ''
            while count != adddigits:
                count += 1
                newFstreet += '0'
            newFstreet += str(deadendcount)    
            deadendcount += 1
            row[3] = newFstreet
            row[5] = row[5].replace("Dead End", newFstreet, 1)
            if row[6] in blockdic:
                row[6] = blockdic[row[6]]

        if thisTint == 'Dead End':
            countlen = len(str(deadendcount))
            adddigits = iddigits - countlen
            count = 0
            newTstreet = ''
            while count != adddigits:
                count += 1
                newTstreet += '0'
            newTstreet += str(deadendcount)
            deadendcount +=1
            row[4] = newTstreet
            row[5] = row[5].replace("Dead End", newTstreet)
            if row[6] in blockdic:
                row[6] = blockdic[row[6]]

        if 'Dead End' in row[6]:
            if rdtypedict[thisRtid] == 1:
                for item in streettable[thisRtid]:
                    if streettable[thisRtid][item]['FROMMEASURE'] <= thisFmeas and (streettable[thisRtid][item]['TOMEASURE'] + 1) >= thisTmeas:
                        stid = item
                        break
                    else:
                        stid = 'ERROR'
                        continue
            else:
                stid = 'Not a Street'
            row[6] = stid

        ucursor.updateRow((row))    
        lastRtid = thisRtid
        lastFmeas = thisFmeas
        lastTmeas = thisTmeas
        lastFstreet = thisFstreet
        lastTstreet = thisTstreet
        lastFint = thisFint
        lastTint = thisTint
del ucursor

deadendcount = 0
lastRtid = 0
blockdic = {}
with arcpy.da.UpdateCursor(block, ['RouteID','FromMeasure','ToMeasure','FromStreet','ToStreet','FromIntersectionID','ToIntersectionID','BlockID'], sql_clause=(None, 'ORDER BY RouteID, FromMeasure')) as ucursor:
    for row in ucursor:
        thisRtid = row[0]
        thisFmeas = row[1]
        thisTmeas = row[2]
        thisFstreet = row[3]
        thisTstreet = row[4]
        thisFint = row[5]
        thisTint = row[6]
        print row

        if thisRtid != lastRtid:
            deadendcount = 1

        if thisFint == 'Dead End':
            countlen = len(str(deadendcount))
            adddigits = iddigits - countlen
            count = 0
            newFstreet = ''
            while count != adddigits:
                count += 1
                newFstreet += '0'
            newFstreet += str(deadendcount)    
            deadendcount += 1
            row[3] = "DEAD END"
            row[5] = newFstreet
            blockdic[row[7]] = row[7].replace("Dead End", newFstreet, 1) 
            row[7] = row[7].replace("Dead End", newFstreet, 1)            

        if thisTint == 'Dead End':
            countlen = len(str(deadendcount))
            adddigits = iddigits - countlen
            count = 0
            newTstreet = ''
            while count != adddigits:
                count += 1
                newTstreet += '0'
            newTstreet += str(deadendcount)
            deadendcount +=1
            row[4] = "DEAD END"
            row[6] = newTstreet
            blockdic[row[7]] = row[7].replace("Dead End", newTstreet) 
            row[7] = row[7].replace("Dead End", newTstreet)
        ucursor.updateRow((row))   
        lastRtid = thisRtid
        lastFmeas = thisFmeas
        lastTmeas = thisTmeas
        lastFstreet = thisFstreet
        lastTstreet = thisTstreet
        lastFint = thisFint
        lastTint = thisTint
del ucursor

deadendcount = 0
lastRtid = 0
with arcpy.da.UpdateCursor(subblock, ['RouteID','FromMeasure','ToMeasure','FromIntersectionID','ToIntersectionID','SubBlockID','BlockID'], sql_clause=(None, 'ORDER BY RouteID, FromMeasure')) as ucursor:
    for row in ucursor:
        thisRtid = row[0]
        thisFmeas = row[1]
        thisTmeas = row[2]
        thisFint = row[3]
        thisTint = row[4]
        print row

        if thisRtid == '11003332' and thisFmeas > 511:
            print "OK"

        if thisRtid != lastRtid:
            deadendcount = 1

        if thisFint == 'Dead End':
            countlen = len(str(deadendcount))
            adddigits = iddigits - countlen
            count = 0
            newFstreet = ''
            while count != adddigits:
                count += 1
                newFstreet += '0'
            newFstreet += str(deadendcount)    
            deadendcount += 1
            row[3] = newFstreet
            row[5] = row[5].replace("Dead End", newFstreet, 1)
            if row[6] in blockdic:
                row[6] = blockdic[row[6]]

        if thisTint == 'Dead End':
            countlen = len(str(deadendcount))
            adddigits = iddigits - countlen
            count = 0
            newTstreet = ''
            while count != adddigits:
                count += 1
                newTstreet += '0'
            newTstreet += str(deadendcount)
            deadendcount +=1
            row[4] = newTstreet
            row[5] = row[5].replace("Dead End", newTstreet)
            if row[6] in blockdic:
                row[6] = blockdic[row[6]]

        if 'Dead End' in row[6]:
            if rdtypedict[thisRtid] == 1:
                for item in streettable[thisRtid]:
                    if streettable[thisRtid][item]['FROMMEASURE'] <= thisFmeas and (streettable[thisRtid][item]['TOMEASURE'] + 1) >= thisTmeas:
                        stid = item
                        break
                    else:
                        stid = 'ERROR'
                        continue
            else:
                stid = 'Not a Street'
            row[6] = stid

        ucursor.updateRow((row))
        lastRtid = thisRtid
        lastFmeas = thisFmeas
        lastTmeas = thisTmeas
        lastFstreet = thisFstreet
        lastTstreet = thisTstreet
        lastFint = thisFint
        lastTint = thisTint
del ucursor
















