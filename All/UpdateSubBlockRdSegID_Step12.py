#----------------------------------------------------
#Updates the RoadwaySegmentID field in the SubBlocks
#----------------------------------------------------

import arcpy, os, sys
import requests
import numpy
import operator
import math

scriptloc = sys.path[0]
os.chdir(scriptloc)
gdb = "scratch.gdb"
scratchgdb = os.path.join(scriptloc, gdb)

connection = r"Database Connections\DDOTLRS as LRSVIEWER.sde"

rdwy = os.path.join(connection, 'Ddotlrs.RH.LRSN_Roadway')
rdsegs = os.path.join(connection, 'Ddotlrs.RH.LRSE_RoadwaySegment')
enhancedsegtbl = os.path.join(scratchgdb, 'RH_SubBlockTable')
sblocklayer = os.path.join(scratchgdb, 'RH_SubBlockLayer')

rdsegdic = {}
with arcpy.da.SearchCursor(rdsegs, ['RouteID','ROADWAYSEGID','FromMeasure','ToMeasure'],where_clause="(FROMDATE is null or FROMDATE<=CURRENT_TIMESTAMP) and (TODATE is null or TODATE>CURRENT_TIMESTAMP)") as scursor:
    for row in scursor:
        rtid = row[0]
        rdsegid = row[1]
        fmeas = row[2]
        tmeas = row[3]

        if rtid not in rdsegdic:
            rdsegdic[rtid] = {}
            rdsegdic[rtid][rdsegid] = {}
            rdsegdic[rtid][rdsegid]['FMeas'] = fmeas
            rdsegdic[rtid][rdsegid]['TMeas'] = tmeas
        else:
            rdsegdic[rtid][rdsegid] = {}
            rdsegdic[rtid][rdsegid]['FMeas'] = fmeas
            rdsegdic[rtid][rdsegid]['TMeas'] = tmeas
del scursor

with arcpy.da.UpdateCursor(enhancedsegtbl, ['RouteID', 'FromMeasure', 'ToMeasure','RoadwaySegID','RouteName']) as ucursor:
    for row in ucursor:
        rtid = row[0]
        fmeas = row[1]
        tmeas = row[2]
        print row

        if row[4] == 'LOIS MAILOU JONES ALY NW':
            continue
        try:
            rtrdsegdic = rdsegdic[rtid]
        except KeyError:
            continue
        for rdseg in rtrdsegdic:            
            if rtrdsegdic[rdseg]['FMeas'] > (fmeas - 1) and rtrdsegdic[rdseg]['TMeas'] < (tmeas + 1):
                row[3] = rdseg
                ucursor.updateRow((row))
            else:
                continue
del ucursor

rdwyq = "rdwyq"
where = "(FROMDATE is null or FROMDATE<=CURRENT_TIMESTAMP) and (TODATE is null or TODATE>CURRENT_TIMESTAMP)"
if arcpy.Exists(rdwyq):
    arcpy.Delete_management(rdwyq)
    arcpy.MakeFeatureLayer_management(rdwy, rdwyq, where)
else:
    arcpy.MakeFeatureLayer_management(rdwy, rdwyq, where)
    
if not arcpy.Exists(sblocklayer):
    arcpy.MakeRouteEventLayer_lr(rdwyq, 'ROUTEID', enhancedsegtbl, 'RouteID LINE FromMeasure ToMeasure', sblocklayer)
else:
    arcpy.Delete_management(sblocklayer)
    arcpy.MakeRouteEventLayer_lr(rdwyq, 'ROUTEID', enhancedsegtbl, 'RouteID LINE FromMeasure ToMeasure', sblocklayer)

sblockfc = os.path.join(scratchgdb, 'RH_SubBlock')
if not arcpy.Exists(sblockfc):
    arcpy.CopyFeatures_management(sblocklayer, sblockfc)
else:
    arcpy.Delete_management(sblockfc)
    arcpy.CopyFeatures_management(sblocklayer, sblockfc)    


print "SCRIPT FINISHED RUNNING"

















