#---------------------------------------------------------
#This script populates the BlockKey and SubBlockKey fields.
#---------------------------------------------------------
import arcpy, os
import os, sys
from pprint import pprint
import requests
import numpy
import operator
import math
import hashlib

scriptloc = sys.path[0]
os.chdir(scriptloc)
gdb = "scratch.gdb"
scratchgdb = os.path.join(scriptloc, gdb)
connection = r"Database Connections\DDOTLRS as LRSVIEWER.sde"
mar_connection = r"Database Connections\GEODEV_DCGIS_USER.sde"
write_connection = r"Database Connections\DDOTLRS as RH user.sde\Ddotlrs.RH.ReferenceFeatures"

marpoints = os.path.join(mar_connection, 'MAR.BLOCKS_PT')
subblock_write = os.path.join(write_connection, 'Ddotlrs.RH.SubBlock')
block_write = os.path.join(write_connection, 'Ddotlrs.RH.Block')
rdwyseg = os.path.join(connection, 'Ddotlrs.RH.LRSE_RoadwaySegment')
subblock = os.path.join(scratchgdb, 'RH_SubBlock')
block = os.path.join(scratchgdb, 'RH_Block')
subblocktbl = os.path.join(scratchgdb, 'RH_SubBlockTable')
blocktbl = os.path.join(scratchgdb, 'RH_BlockTable')

mar_dict = {}
with arcpy.da.SearchCursor(marpoints, ['BLOCKKEY','BLOCK_NAME']) as scursor:
    for row in scursor:
        blockkey = row[0]
        blockname = row[1]
        mar_dict[blockkey] = blockname
del scursor

print "Updating SubBlocks..."
with arcpy.da.UpdateCursor(subblocktbl, ['SubBlockID','BlockID','FromIntersectionID','ToIntersectionID','SubBlockKey','BlockKey','FromIntersectionKey','ToIntersectionKey']) as ucursor:
    for row in ucursor:
        if row[0] == None or row[0] == '':
            continue
        if row[1] == None or row[1] == '':
            continue
    #SubBlockID to SubBlockKey
        subblockHas = hashlib.md5(row[0])
        subblockKey = str(subblockHas.hexdigest())
        row[4] = subblockKey
    #BlockID to BlockKey
        blockHas = hashlib.md5(row[1])
        blockKey = str(blockHas.hexdigest())
        row[5] = blockKey
    #FromIntersectionID to FromIntersectionKey
        fintHas = hashlib.md5(row[2])
        fintKey = str(fintHas.hexdigest())
        row[6] = fintKey
    #ToIntersectionID to ToIntersectionKey
        tintHas = hashlib.md5(row[3])
        tintKey = str(tintHas.hexdigest())
        row[7] = tintKey

        ucursor.updateRow((row))
del ucursor

print "Updating Blocks..."
with arcpy.da.UpdateCursor(blocktbl, ['BlockID', 'FromIntersectionID', 'ToIntersectionID', 'BlockKey', 'FromIntersectionKey', 'ToIntersectionKey','HundredBlock']) as ucursor:
    for row in ucursor:
        if row[0] == None or row[0] == '':
            continue
    #BlockID to BlockKey
        blockHas = hashlib.md5(row[0])
        blockKey = str(blockHas.hexdigest())
        row[3] = blockKey
    #FromIntersectionID to FromIntersectionKey
        fintHas = hashlib.md5(row[1])
        fintKey = str(fintHas.hexdigest())
        row[4] = fintKey
    #ToIntersectionID to ToIntersectionKey
        tintHas = hashlib.md5(row[2])
        tintKey = str(tintHas.hexdigest())
        row[5] = tintKey
        try:
            row[6] = mar_dict[blockKey]
        except KeyError:
            row[6] = None
        ucursor.updateRow((row))
del ucursor

print "Updating SubBlocks..."
with arcpy.da.UpdateCursor(subblock, ['SubBlockID','BlockID','FromIntersectionID','ToIntersectionID','SubBlockKey','BlockKey','FromIntersectionKey','ToIntersectionKey']) as ucursor:
    for row in ucursor:
        if row[0] == None or row[0] == '':
            continue
        if row[1] == None or row[1] == '':
            continue
    #SubBlockID to SubBlockKey
        subblockHas = hashlib.md5(row[0])
        subblockKey = str(subblockHas.hexdigest())
        row[4] = subblockKey
    #BlockID to BlockKey
        blockHas = hashlib.md5(row[1])
        blockKey = str(blockHas.hexdigest())
        row[5] = blockKey
    #FromIntersectionID to FromIntersectionKey
        fintHas = hashlib.md5(row[2])
        fintKey = str(fintHas.hexdigest())
        row[6] = fintKey
    #ToIntersectionID to ToIntersectionKey
        tintHas = hashlib.md5(row[3])
        tintKey = str(tintHas.hexdigest())
        row[7] = tintKey

        ucursor.updateRow((row))
del ucursor

print "Updating Blocks..."
with arcpy.da.UpdateCursor(block, ['BlockID', 'FromIntersectionID', 'ToIntersectionID', 'BlockKey', 'FromIntersectionKey', 'ToIntersectionKey','HundredBlock']) as ucursor:
    for row in ucursor:
        if row[0] == None or row[0] == '':
            continue
    #BlockID to BlockKey
        blockHas = hashlib.md5(row[0])
        blockKey = str(blockHas.hexdigest())
        row[3] = blockKey
    #FromIntersectionID to FromIntersectionKey
        fintHas = hashlib.md5(row[1])
        fintKey = str(fintHas.hexdigest())
        row[4] = fintKey
    #ToIntersectionID to ToIntersectionKey
        tintHas = hashlib.md5(row[2])
        tintKey = str(tintHas.hexdigest())
        row[5] = tintKey
        try:
            row[6] = mar_dict[blockKey]
        except KeyError:
            row[6] = None
        ucursor.updateRow((row))
del ucursor

print 'Clearing out previous subblock data...'
arcpy.TruncateTable_management(subblock_write)

print 'Appending updated subblock data to SDE...'
arcpy.Append_management(subblock, subblock_write, "NO_TEST")

print 'Clearing out previous block data...'
arcpy.TruncateTable_management(block_write)

print 'Appending updated block data to SDE...'
arcpy.Append_management(block, block_write, "NO_TEST")








