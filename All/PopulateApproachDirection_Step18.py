#--------------------------------------
#Populates IntersectionDirection field.
#--------------------------------------

import arcpy, os, sys
from math import *
import requests
import numpy
import operator

def FindIncomingDirection(intid, appid, rtid, fmeas, tmeas):
    fmeas = round(fmeas, 3)
    tmeas = round(tmeas, 3)

    try:
        intlist = rtintdic[rtid]
    except KeyError:
        intlist = [0]
    if fmeas in intlist or round(fmeas + .001,3) in intlist or round(fmeas + .002,3) in intlist or round(fmeas - .001,3) in intlist or round(fmeas - .002,3) in intlist:
        return 2
    elif tmeas in intlist or round(tmeas + .001,3) in intlist or round(tmeas + .002,3) in intlist or round(tmeas - .001,3) in intlist or round(tmeas - .002,3) in intlist:
        return 1
    else:
        return 0

scriptloc = sys.path[0]
os.chdir(scriptloc)
gdb = "scratch.gdb"
scratchgdb = os.path.join(scriptloc, gdb)

connectiondev = r"Database Connections\DDOTLRS as LRSVIEWER.sde"
connection = r"Database Connections\DDOTLRS as CARSSVIEWER.sde"
write_connection = r"Database Connections\DDOTLRS as RH user.sde\Ddotlrs.RH.ReferenceFeatures"

routeintfc = os.path.join(connectiondev, 'RH.LRSI_RouteIntersection')
rdwy = os.path.join(connectiondev,'Ddotlrs.RH.LRSN_Roadway')
approach_sde = os.path.join(write_connection,'Ddotlrs.RH.Approach')
intapproaches = os.path.join(scratchgdb, 'RH_Approaches')
rhrdwyseg = os.path.join(scratchgdb, 'RH_SubBlock')


with arcpy.da.SearchCursor(routeintfc, ['ROUTEID','MEASURE']) as scursor:
    rtintdic = {}
    for row in scursor:
        rtid = row[0]
        meas = round(row[1],3)
        if rtid in rtintdic:
            rtintdic[rtid].append(meas)
        else:
            rtintdic[rtid] = []
            rtintdic[rtid].append(meas)

with arcpy.da.UpdateCursor(intapproaches, ['RouteID','IntersectionID','ApproachID','IntersectionDirection','FromMeasure','ToMeasure']) as ucursor:
    for row in ucursor:
        rtid = row[0]
        intid = row[1]
        appid = row[2]
        frmeas = row[4]
        tomeas = row[5]
        print intid, appid
        if intid == '11000902_11058242' and appid == 181:
            print "OK"
            
        appdir = FindIncomingDirection(intid, appid, rtid, frmeas, tomeas)
        row[3] = appdir
        ucursor.updateRow((row))


#ROUTES TABLE TO ROADWAYS    
rdwyq = "rdwyq"
where = "(FROMDATE is null or FROMDATE<=CURRENT_TIMESTAMP) and (TODATE is null or TODATE>CURRENT_TIMESTAMP)"
approachlayer = os.path.join(scratchgdb, 'ApproachLayer')

print 'Making feature layer...'
if arcpy.Exists(rdwyq):
    arcpy.Delete_management(rdwyq)
    arcpy.MakeFeatureLayer_management(rdwy, rdwyq, where)
else:
    arcpy.MakeFeatureLayer_management(rdwy, rdwyq, where)

print 'Making route event layer...'
if not arcpy.Exists(approachlayer):
    arcpy.MakeRouteEventLayer_lr(rdwyq, 'ROUTEID', intapproaches, 'RouteID LINE FromMeasure ToMeasure', approachlayer)
else:
    arcpy.Delete_management(approachlayer)
    arcpy.MakeRouteEventLayer_lr(rdwyq, 'ROUTEID', intapproaches, 'RouteID LINE FromMeasure ToMeasure', approachlayer)

print 'Making feature class...'
approachfc = os.path.join(scratchgdb, 'ApproachFC')
if not arcpy.Exists(approachfc):
    arcpy.CopyFeatures_management(approachlayer, approachfc)
else:
    arcpy.Delete_management(approachfc)
    arcpy.CopyFeatures_management(approachlayer, approachfc)
#########################

#UPDATES SDE PARKING ZONES
print 'Clearing out previous parking zone data...'
arcpy.TruncateTable_management(approach_sde)

print 'Appending updated parking zone data to SDE...'
arcpy.Append_management(approachfc, approach_sde, "NO_TEST")

arcpy.Delete_management(approachlayer)
arcpy.Delete_management(approachfc)

print "FINISHED"